;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; string munging
(defun wiki-+-uglify-string (string)
  "Take a \"foo bar\" or \"foo_bar\" string and return its wiki word form."
  (if (string-match "^'\\(.+\\)" string)
      (match-string 1 string)
    (apply 'concat (split-string (upcase-initials string) "[ _]+")) ))

(defun wiki-+-fix-string (string)
  "Take a wiki word and expand into something readable."
  (let ((case-fold-search nil))
    (replace-regexp-in-string
     "\\([a-z]\\)\\([A-Z]\\)" "\\1 \\2" string :fixed-case) ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; + wiki support: extracting values and textarea values from a parsed form:
(defun wiki-+-form-values (form)
  "Return an alist of (name . value) entries representing a form (not including 
textarea elements).\n
FORM should be an xml.el style list, simplified to remove any non-form nodes
\(such as divs, tables and other non-form-related elements).\n
The name and value in each cons are usually both strings.
In the case of select elements, the value may be a list of strings.
See also: `wiki-+-form-texts'"
  (delete nil
   (mapcar (lambda (e)
             (when (not (eq (xml-node-name e) 'textarea))
               (wiki-xml-form-element-value e))) (xml-node-children form)) ))

(defun wiki-+-form-texts (form)
  "An alist of (name . value) entries representing the textareas in a form.
See also `wiki-+-form-values'.\n
FORM should be an xml.el style list, simplified to remove any non-form nodes.\n
The name and value in each cons are strings."
  (delete nil
   (mapcar (lambda (e)
             (when (eq (xml-node-name e) 'textarea)
               (wiki-xml-form-element-value e))) (xml-node-children form)) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki table helper functions:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun  wiki-+-table-separator () "||")
(defvar wiki-+-table-seek-table-re nil)
(defvar wiki-+-table-seek-row-re   nil)
(defvar wiki-+-table-seek-col-re   nil)
(defvar wiki-+-table-row-end-re    nil)

(defun wiki-+-table-seek-table-re ()
  "Regex used by the table functions to help find the start of a table."
  (let ((re (wiki-var 'table-seek-table-re)))
    (or (symbol-value re)
        (set re
             (format "^[ \t]*\\(%s\\)" 
                     (funcall (wiki-fun 'table-separator))) )) ))

(defun wiki-+-table-seek-row-re ()
  "Regex use by the table functions to help find table row data."
  (let ((re (wiki-var 'table-seek-row-re)))
    (or (symbol-value re) 
        (set re 
             (format "^[ \t]*\\(%s.+\\)" 
                     (funcall (wiki-fun 'table-separator))) )) ))

(defun wiki-+-table-seek-col-re ()
  "Regex used by the table functions to help find columns."
  (let ((re (wiki-var 'table-seek-col-re)))
    (or (symbol-value re) 
        (set re (format "^[ \t]*%s" 
                        (funcall (wiki-fun 'table-separator))) )) ))

(defun wiki-+-table-row-end-re ()
  "Regex used to test whether a row has the final (trailing) column delimiter."
  (let ((re (wiki-var 'table-row-end-re)))
    (or (symbol-value re)
        (set re (format ".*%s\\s-*$" 
                        (funcall (wiki-fun 'table-separator))) )) ))

(defun wiki-+-table-seek-start ()
  "Move to the start of the current table."
  (let ((goal nil) (re (funcall (wiki-fun 'table-seek-table-re))))
    (save-excursion
      (while (and (= 0 (forward-line -1)) 
                  (message "looking for %S at %S" re (point))
                  (looking-at re))
	(setq goal (match-beginning 1)) ))
    (if (not goal)
	(save-excursion
	  (beginning-of-line)
	  (if (looking-at re)
              (setq goal (match-beginning 1)) ) ))
    (and goal (goto-char goal)) ))

(defun wiki-+-table-headers ()
  "Return as a list of strings the contents of the header columns of the
current table \(sans markup and leading/trailing whitespace\)."
  (save-excursion
    (and (funcall (wiki-fun 'table-seek-start))
	 (funcall (wiki-fun 'table-row-data  ))) ))

(defun wiki-+-table-row-data (&optional cellfun)
  "Process the current table row, returning a list consisting of the contents
of each cell. Actually, this applies CELLFUN to each cell\'s data and returns
the result of that call in the place of the cell\'s data. If CELLFUN is
unspecified or nil, strips wiki markup and leading/trailing whitespace from
the cell\'s data. To get the raw data, use a CELLFUN of `identity'."
  (let ( (table-row nil)
	 (cells     nil) 
         (sep (funcall (wiki-fun 'table-separator  )))
         (re  (funcall (wiki-fun 'table-seek-row-re))) )
    (save-excursion
      (beginning-of-line)
      (when (and (looking-at re)
		 (setq table-row (match-string 1)))
	(set-text-properties 0 (length table-row) nil table-row)
	(setq table-row (replace-regexp-in-string "\\s-*$" "" table-row)
	      cells     (split-string table-row sep t)) ) )
    (setq cellfun 
	  (or cellfun
	      (lambda (C) 
                (wiki-trim-whitespace C )) ))
;;              (wiki-trim-whitespace (wiki_strip_markup C) )) ))
    (mapcar cellfun cells) ))

(defun wiki-+-table-data (&optional cellfun rowfun)
  "Return a list of row contents for the current table, each element of the
list being a list of cell contents for that row. CELLFUN is passed to
`wiki-+-table-row-data'. ROWFUN is called on each row list generated by 
`wiki-+-table-row-data', the actual value returned for each row
is the return value of this list. ROWFUN defaults to `identity'."
  (let ((row nil) (table-data nil))
    (or rowfun (setq rowfun 'identity))
    (save-excursion
      (when (funcall (wiki-fun 'table-seek-start))
	(while (and (not (eobp))
		    (setq row (funcall (wiki-fun 'table-row-data) cellfun)))
	  (setq table-data (cons (funcall rowfun row) table-data))
	  (forward-line 1) )))
    (nreverse table-data)))

(defun wiki-+-table-column-data (col &optional cellfun)
  "Get the column data for column COL as a list. 
See `wiki-+-table-row-data' for an explanation of CELLFUN."
  (funcall (wiki-fun 'table-data) cellfun (lambda (row) (nth (1- col) row))))

(defun wiki-+-table-column-number (point)
  "Return the table column number at the position POINT. Column 0 is the
non-column before the table actually starts. The partial column at the end
of the row \(the one without a trailing ||\) is counted as a full column."
  (interactive "d")
  (save-excursion 
    (beginning-of-line)
    (when (looking-at (funcall (wiki-fun 'table-seek-col-re)))
      (let ((column 0) (sep (funcall (wiki-fun 'table-separator))))
        (while (search-forward sep point :no-error)
          (setq column (1+ column))) column)) ))

(defun wiki-+-table-seek-column (col-number)
  "Move to the beginning of column COL-NUMBER on this row."
  (let ( (goal nil) (end nil) (nth 0) 
         (sep (funcall (wiki-fun 'table-separator))) )
    (save-excursion
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      (when (looking-at (funcall (wiki-fun 'table-seek-col-re)))
	(while (and (> col-number nth) (search-forward sep end :no-error))
	  (setq nth (1+ nth)) )
	(setq goal (point)) ))
    (and goal (goto-char goal)) ))

(defun wiki-+-table-count-columns ()
  "Return the number of columns \(including the trailing partial column\)."
  (save-excursion 
    (end-of-line) 
    (funcall (wiki-fun 'table-column-number) (point)) ))

(defun wiki-+-table-pad-row (n)
  "Pad the current row to at least N columns, inserting placeholder cells.
Returns the number of columns in the row after any padding is done."
  (let ( (cols (funcall (wiki-fun 'table-count-columns))) 
         (end  (concat " " (funcall (wiki-fun 'table-separator))))
         (cell (concat " <%s> " (funcall (wiki-fun 'table-separator))))
         (head nil)
         (full nil) )
    (cond ((not  cols)  nil)
          ((>= cols n) cols)
          (t (setq head (funcall (wiki-fun 'table-headers)))
             (beginning-of-line)
             (setq full (looking-at (funcall (wiki-fun 'table-row-end-re))))
             (end-of-line)
             (if full nil (insert end) (setq cols (+ 1 cols)))
             (while (> (- n cols) 0)
               (insert (format cell (nth (1- cols) head)))
               (setq cols (+ 1 cols))) cols) )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; interactive table ui functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-+-table-add-row ()
  "Add a table row immediately after the current table row, or at the
end of the table if we are on a line immediately following a table."
  (interactive)
  (let ( (cols (funcall (wiki-fun 'table-headers  ))) 
         (sep  (funcall (wiki-fun 'table-separator))) )
    (when cols
      ;; put the point at the beginning of the line after this row, or the 
      ;; beginning of the line after the last row if we are not in a row.
      (beginning-of-line)
      (when (and (looking-at (funcall (wiki-fun 'table-seek-col-re)))
                 (progn (end-of-line) (= 1 (forward-line 1))) )
	(insert "\n"))
      (insert sep " <" (wiki-join (concat "> " sep " <") cols) "> " sep "\n")
      (forward-line -1) )) )

(defun wiki-+-table-add-column (name)
  "Add a column with a head cell containing NAME and each subsequent
row containing <NAME> \(in case the table is unaligned\) after the
current column."
  (interactive "scolumn-name: ")
  (let ( (col (1+ (funcall (wiki-fun 'table-column-number) (point))))
         (str (format " <%s> %s" name (funcall (wiki-fun 'table-separator)))) )
    (end-of-line)
    (if (< (funcall (wiki-fun 'table-column-number) (point)) col)
	(setq col (- col 1)))
    (funcall (wiki-fun 'table-seek-start))
    (while (and (not (eobp)) (funcall (wiki-fun 'table-seek-column) col))
      (funcall (wiki-fun 'table-pad-row) col)
      (insert str)
      (forward-line 1)) ))

(defun wiki-+-table-del-column ()
  "Delete the current column from the table. If you are before the first
column, the first column will be deleted instead. If you are after the
last, nothing will be removed."
  (interactive)
  (let ( (col (funcall (wiki-fun 'table-column-number) (point))) (move 0) 
         (sep (funcall (wiki-fun 'table-separator))) )
    (if (= col 0) (setq col 1))
    (funcall (wiki-fun 'table-seek-start))
    (while (and (= move 0) (funcall (wiki-fun 'table-seek-column) col))
      (backward-char 2)
      (if (looking-at (concat "\\(" sep ".+?\\)" sep))
	  (delete-region (match-beginning 1) (match-end 1)))
      (end-of-line)
      (setq move (forward-line)) ) ))

(defun wiki-+-table-align ()
  "Align the current table, sort of, ignoring cells that are too big."
  (interactive)
  (or (funcall (wiki-fun 'table-seek-start)) 
      (error "wiki: No table found"))
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (let ( (table (funcall (wiki-fun 'table-data) 'wiki-trim-whitespace))
         (sep   (funcall (wiki-fun 'table-separator)))
         (ncols   nil)   ;; (max) number of columns 
         (col-len nil)   ;; column lengths (space trimmed) 
         (col-nzl nil)   ;; ditto, excluding zero-length 
         (col-fmt nil)   ;; list of format strings 
         (fmt-lst nil)   ;; temp pointer to same 
         (x         0) ) ;; 
    (setq ncols (apply 'max (mapcar 'length table)))
    ;; iterate over the columns:
    (while (< x ncols)
      ;; work out the list of column lengths, and the list excluding
      ;; 0 length columns:
      (setq col-len (mapcar (lambda (row) (length (nth x row))) table)
	    col-nzl (apply 'append
			   (mapcar
			    (lambda (l) (if (= l 0) nil (list l))) col-len)))

      ;; work out the maximum, standard deviation and mean of the length
      ;; for this column. We fiddle the mean by excluding 0 length columns.
      (let ( (max (apply 'max col-len))
	     (std (sqrt (wiki-sample-variance col-len)))
	     (avg (wiki-mean col-nzl))
	     (len nil) )
	;; ideal column length is either maximum or mean + 3 std deviations
	(setq len (min max (+ (* std 3) avg)))
	;;(message "-%S: %d ±%d [%d]" col-len avg std max)
	;; if some columns would be chopped out by this, exclude them and
	;; recalculate the maximum, using this trimmed max as the ideal length:
	(if (> max len)
	  (setq col-nzl
		(apply 'append
		       (mapcar
			(lambda (l) (if (> l len) nil (list l))) col-nzl))
		len (apply 'max col-nzl)))
	;; record the ideal format for this column
	(setq fmt (format " %%-%ds %s" len sep)
	      col-fmt (cons fmt col-fmt)) )
      (setq x (1+ x)) )
    ;; whoops, we build these up in reverse order because it's easier.
    (setq col-fmt (nreverse col-fmt))
    ;; we are at the start of the table: kill the old table:
    (beginning-of-line)
    (let* ( (jmp (length table))
	    (beg (point))
	    (end (line-end-position jmp)) )
      (delete-region beg end))
    ;; print out the new one:
    (mapcar
     (lambda (row)
       (insert sep)
       (setq fmt-list col-fmt)
       (mapcar (lambda (cell)
		 (insert (format (car fmt-list) cell))
		 (setq fmt-list (cdr fmt-list))) row) (insert "\n")) table))
  (delete-char -1))

(defun wiki-+-table-whereami ()
  "Give the user a hint as to their current position within a wiki table."
  (interactive)
  (let ( (col  (funcall (wiki-fun 'table-column-number) (point)))
	 (head (funcall (wiki-fun 'table-headers))) 
         (row  (funcall (wiki-fun 'table-row-data))) )
    (if (and col (< 0 col) head row)
	(message "You are in column #%d [%s x %s]" 
                 col (nth (- col 1) head) (car row))
      (message "You are in a maze of markup, all alike. [%S,%S]" col head)) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki buffer navigation and ui functions
(defconst wiki-+-xlink-regex "\\<\\([a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\)\\>")
(defun wiki-+-target-at-point (point)
  "Return the string surrounding POINT if it is a wiki link, or nil."
  (save-excursion
    (let ((end nil) (x t) (target nil) (case-fold-search nil))
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      ;; search forwards till we find an xref that spans point:
      (while (and x (search-forward-regexp wiki-xlink-regex end :no-error))
	(and (<= (match-beginning 1) point)
	     (>= (match-end       1) point)
	     (setq x nil target (match-string 1)) ))
      (concat "'" target)) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; loading related functions
(defun wiki-+-fix-bogus-xhtml () 
  "Fix up bogus xhtml in the current buffer to make `xml-parse-region' safe."
  t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; saving related functions
(defun wiki-+-save-url (form url)
  "Where FORM is an xml.el style list containing the XHTML form data
\(simplified to remove any non-form elements, see `wiki-xml-form-data') 
and url is a url.el style url-vector (see `url-generic-parse-url'), construct
or extract the target URL for the wiki save operation, and return it."
  (let ( (url-string  (cdr (assq 'action (xml-node-attributes form))))
         (url-vector  nil) )
    (setq url-vector (url-generic-parse-url url-string))

    (if (> 1 (length (url-filename url-vector))) 
        (url-set-filename url-vector "/"))

    (setf (url-fullness url-vector) t)
    
    (when url
      (if (> 1 (length (url-host url-vector))) 
          (url-set-host url-vector (url-host url)))
      (if (> 1 (length (url-type url-vector))) 
          (url-set-type url-vector (url-type url)))
      (if (or (integerp (url-port url-vector))
              (> 1 (length (url-port url-vector))))
          (url-set-port url-vector (url-port url))) )

    (url-recreate-url url-vector)))

(defun wiki-+-save ()
  "Save the current buffer's wiki page. Display the response from the
wiki server when done. \(Ideally just the relevant text, but if wiki.el
is uncertain of the response, it will barf up the whole response for you
to see\)."
  (interactive)
  (if (not wiki-edit-data) 
      (save-buffer)
    (let ( (save-text (wiki-parse-buffer))
           (save-vals (cdr (assoc :values wiki-edit-data)))
           (form      (cdr (assoc :form   wiki-edit-data)))
           (orig-url  (cdr (assoc :url    wiki-edit-data)))
           (page      (cdr (assoc :title  wiki-edit-data)))
           (callback  (wiki-fun 'response-callback))
           (get-url   (wiki-fun 'save-url))
           (wiki-buf  (current-buffer))
           (postdata  nil) )
      (setq postdata
            (apply 'concat
                   (mapcar
                    (lambda (X)
                      (apply (lambda (A B) (concat "&" A "=" B))
                             (wiki-query-encode (list (car X) (cdr X)))))
                    (append save-text save-vals '(("dummy" . "value")))) ) )
      (setq postdata (substring postdata 1)
            postdata (concat postdata "\n"))
      (let ( (url-request-data   postdata)
             (url-request-method "POST"  )
             (url-request-extra-headers
              '( ("Content-Type" . "application/x-www-form-urlencoded") )) )
        (wiki-fetch (funcall get-url form orig-url)
                    callback (list  page  wiki-buf))) ) ))


(defun wiki-+-response-callback (&optional status-list page buffer)
  "Recreate the magic buffer-local `wiki-name' and `wiki-flavour' variables
and then call the `wiki-+-display-response' callback."
  (let (flavour name cbuf)
    (setq cbuf (current-buffer))
    (set-buffer buffer) (setq flavour wiki-flavour name wiki-name)
    (set-buffer cbuf  ) (setq wiki-flavour flavour wiki-name name)
    (message "wiki-response-callback: %S %S %S" buffer wiki-flavour wiki-name)
    (funcall (wiki-fun 'display-response) status-list page buffer)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ui vars
(defvar wiki-+-target-history nil)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pull it all together into a mode:
(defun wiki-+-mode-map ()
  "Return a suitable keymap for the current wiki flavour."
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map text-mode-map)
    (define-key map (kbd "C-x C-s")   (wiki-fun 'save))
    (define-key map (kbd "C-c C-c")   (wiki-fun 'save))
    (define-key map (kbd "C-c C-f")   'wiki-follow)
    (define-key map (kbd "C-c t r")   (wiki-fun 'table-add-row))
  ;;(define-key map (kbd "C-c t R")   (wiki-fun 'table-del-row))
    (define-key map (kbd "C-c t c")   (wiki-fun 'table-add-column))
    (define-key map (kbd "C-c t k")   (wiki-fun 'table-del-column))
    (define-key map (kbd "C-c t TAB") (wiki-fun 'table-align))
    (define-key map (kbd "C-c t ?")   (wiki-fun 'table-whereami))
    (define-key map (kbd "C-c d i")   'wiki-idiff)
    (define-key map (kbd "C-c d =")   'wiki-diff)
    map))

(defvar wiki-+-mode-syntactic-keywords
  '(("{{{" (0 "|" keep))    ;; generic string fence 1
    ("}}}" (0 "|" keep))) ) ;; generic string fence 2

(provide 'wiki-+)
