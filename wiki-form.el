;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

;; (defun html-to-form-data (file)
;;   (interactive "f(x)html file: ")
;;   (set-buffer (find-file-noselect file))
;;   (setq vdforms
;;         (wiki-xml-form-data (xml-parse-region (point-min) (point-max))) ))

(defvar wiki-form-skip-elements nil)

(defvar wiki-form-cbradio-state nil
  "A cons of (NAME-STRING . TYPE-STRING), indicating the checkbox/radio
get currently being accumulated or dealt with.
See also: `wiki-form-cbradio-data'.")

(defvar wiki-form-cbradio-data  nil
  "An alist of (VALUE . XML-ATTR-ALIST) for the current checkbox/radio widget.
See also: `wiki-form-cbradio-state'.")

(defvar wiki-form-ui-data       nil
  "A list of the top-level widgets that make up the simplified representation
of the xhtml form being presented to the user in the current buffer.")

(defvar wiki-form-xml-data      nil
  "A (simplified) xml.el style structure representing an xhtml form, such as
thise making up the elements of the list returned by `wiki-xml-form-data'.")

;; this isn't actually needed, but is a handy diagnostic, as the value actually
;; shows up when browsing the widget properties with describe-text-properties
(defun wiki-form-ui-handle-event (&optional parent child event &rest stuff)
  (widget-put parent :value (widget-value parent)))

(defun wiki-form-element-insert-current-cbradio ()
  "Take the details of the last complete checkbox/radio group
\(found in `wiki-form-cbradio-data' and `wiki-form-cbradio-state') and
render them into the buffer as appropriate widget(s).
Called by `wiki-form-xml-data-to-ui' and `wiki-form-element-input-to-ui',
the latter being responsible for loading the right data into the two state
variables mentioned above."
  (when (and wiki-form-cbradio-state wiki-form-cbradio-data)
    (let ((name (car wiki-form-cbradio-state))
          (type (cdr wiki-form-cbradio-state))
          (value nil))
      (mapc
       (lambda (item)
         (and (assq 'checked (cdr item)) (setq value (cons (car item) value))))
       wiki-form-cbradio-data)
      (apply 'wiki-form-create-widget
             (if (equal type "checkbox") 'set 'radio)
             (cons name type)
             :value   (if (equal type "checkbox") value (car value))
             :tag     name
             :notify 'wiki-form-ui-handle-event
             (mapcar
              (lambda (c)
                (list 'const :tag (car c) :value (car c)))
              (nreverse wiki-form-cbradio-data)) ))
    (widget-insert "\n---\n")) )

(defun wiki-form-element-input-to-ui (element tag attr name value)
  "Render an xhtml <input ...> form element into emacs widgets.
ELEMENT is an xml.el style structure representing the element.
TAG is a symbol representing the xhtml tag (always 'input in this case).
ATTR is the xml.el style xhtml attribute alist for the element.
NAME is a string giving the name of the input element.
VALUE is a string giving the value of the element."
  (let ( (type (cdr (assq 'type attr))) )
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; already traversing a checkbox/radio group
    (when wiki-form-cbradio-state
      (if (equal wiki-form-cbradio-state (cons name type))
          ;; still in the same cbradio group as before, accumulate node data
          (setq wiki-form-cbradio-data
                (cons (cons value attr) wiki-form-cbradio-data))
        ;; we've left the group. render the checkbox/radio group
        ;; we can do this as the wiki element harvesting code guarantees
        ;; that checkbox/radio groups are contiguous
        (wiki-form-element-insert-current-cbradio)
        (setq wiki-form-cbradio-state nil wiki-form-cbradio-data nil)))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; in the next case we are not currently in a checkbox/radio group: either
    ;; because we genuinely aren't, or because we finished the last group above:
    (when (and (not wiki-form-cbradio-data)
               (member type '("checkbox" "radio")))
      ;; if we have entered a new group, start accumulating:
      (setq wiki-form-cbradio-state (cons name type)
            wiki-form-cbradio-data
            (cons (cons value attr) wiki-form-cbradio-data)))
    (cond
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((member type '("text" "password"))
      (let ( (secret (if (equal type "password") ?* nil))
             (size   (or (cdr (assq 'size attr)) 20)) )
        (when (stringp size) (setq size (string-to-int size)))
        (wiki-form-create-widget 'string
                                 (cons name type)
                                 :notify 'wiki-form-ui-handle-event
                                 :tag    name
                                 :size   size
                                 :secret secret
                                 value))
      (widget-insert "\n---\n"))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((equal type "hidden")
      (wiki-form-create-widget 'wiki-const (cons name type) value))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((member type '("submit" "image"))
      (wiki-form-create-widget 'push-button
                               (cons name type)
                               :notify 'wiki-form-ui-submit
                               :format (format "[%%[%s%%]]" value)
                               :tag    name
                               value)
      (widget-insert "\n---\n"))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((equal type "reset")
      (wiki-form-create-widget 'push-button
                               (cons name type)
                               :format (format "[%%[%s%%]]" value)
                               :tag    name
                               :notify 'wiki-form-to-ui-reset
                               value)
      (widget-insert "\n---\n"))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; these two are actually dealt with above, as they are not single
     ;; nodes but separate nodes (guaranteed consecutive by wiki-xml-form-data)
     ;; whose state must be figured out by seeing when we run out of nodes
     ;; belonging to a given group:
     ((equal type "checkbox") nil)
     ((equal type "radio"   ) nil)
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((equal type "hidden"  ) nil)
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     (t (message "ignoring input type=%S" type)) ) ))

(defun wiki-form-element-select-to-ui (element tag attr name value)
  "Render an xhtml <select ...> element into emacs wisgets.
ELEMENT is an xml.el style structure representing the element.
TAG is a symbol representing the xhtml tag (always 'select in this case).
ATTR is the xml.el style xhtml attribute alist for the element.
NAME is a string giving the name of the input element.
VALUE is a string giving the value of the element."
  (let (options txt val sel)
      (setq options
            (mapcar
             (lambda (option)
               (let (txt val sel)
                 (setq txt (wiki-trim-whitespace (wiki-xml-node-text option))
                       val (cdr (assq 'value    (xml-node-attributes option)))
                       sel (cdr (assq 'selected (xml-node-attributes option))))
                 (cons sel (cons (or (and (< 0 (length txt)) txt) val)
                                 (or val txt))) ))
             (xml-node-children element) ))
      (if (cdr (assq 'multiple attr))
          (apply 'wiki-form-create-widget
                 'set
                 (cons name type)
                 :format "%{%t%}:\n%v"
                 :tag     name
                 :value   value
                 :notify 'wiki-form-ui-handle-event
                 (mapcar
                  (lambda (o)
                    (list 'const :tag (cadr o) (cddr o))) options) )
        (apply 'wiki-form-create-widget
               'choice
               (cons name type)
               :format "%[%t%] %v"
               :value   value
               :tag     name
               (mapcar
                (lambda (o) (list 'const :tag (cadr o) (cddr o))) options))) )
      (widget-insert "\n---\n"))

(defun wiki-form-element-textarea-to-ui (element tag attr name value)
  "Render an xhtml <textarea ...>...</textarea> element into emacs wisgets.
ELEMENT is an xml.el style structure representing the element.
TAG is a symbol representing the xhtml tag (always 'textarea in this case).
ATTR is the xml.el style xhtml attribute alist for the element.
NAME is a string giving the name of the textarea element.
VALUE is not used in this function."
  (let ((content (replace-regexp-in-string "\\`\\s-+\\|\\s-+\\'" ""
                                           (wiki-xml-node-text element))))
    (wiki-form-create-widget
     'text
     (cons name type)
     :notify 'wiki-form-ui-handle-event
     :tag name content)))

(defun wiki-form-element-to-ui (element)
  "Take an xml.el style structure representing an xhtml form element
and render it into emacs widgets.
checkbox and radio widgets are a special case, and are handled by accumulating
all the checkboxes or radio elements in a given group and rendering them
together only when the group is complete. (In order for this to work, all the
radio or checkbox widgets must be passed to this function in sequence, ie one
after another without any other intervening elements)."
  (let (tag attr name value elem type options)
    (setq tag   (xml-node-name element)
          attr  (xml-node-attributes element)
          elem  (wiki-xml-form-element-value element)
          name  (car elem)
          value (cdr elem))
    (cond
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((eq tag 'input)
      (wiki-form-element-input-to-ui element tag attr name value))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((eq tag 'select)
      (wiki-form-element-select-to-ui element tag attr name value))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ((eq tag 'textarea)
      (wiki-form-element-textarea-to-ui element tag attr name value))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     (t (message "ignoring %S" tag) nil)) ))

(defun wiki-form-create-widget (type ident &rest args)
  "Create an emacs widget, and record it in `wiki-form-ui-data'."
  (let ((widget nil))
    (setq widget            (apply 'widget-create type args)
          wiki-form-ui-data (cons widget wiki-form-ui-data))
    (widget-put widget 'ident ident)
    widget))

(defun wiki-form-xml-data-to-ui (form &optional buffer skip)
  "Take a simplified xml.el style structure FORM representing an xhtml form,
such as one of the elements of the list returned by `wiki-xml-form-data',
and render it as emacs widgets in BUFFER, or in a new buffer if BUFFER is not
supplied."
  (interactive)
  (switch-to-buffer (or buffer (generate-new-buffer "*wiki form*")))
  (kill-all-local-variables)
  (set (make-local-variable 'wiki-form-xml-data) form)
  (set (make-local-variable 'wiki-form-ui-data )  nil)
  (let ((inhibit-read-only t)) (erase-buffer))
  (remove-overlays)
  (let ((wiki-form-cbradio-state  nil)
        (wiki-form-cbradio-data   nil)
        (wiki-form-skip-elements skip))
    (mapcar 'wiki-form-element-to-ui (xml-node-children form))
    (wiki-form-element-insert-current-cbradio))
  (use-local-map widget-keymap)
  (widget-setup)
  (current-buffer))

(defun wiki-form-ui-submit (&optional parent child event &rest stuff)
  (let ( (url-request-extra-headers url-request-extra-headers) 
         name-value encoded f-attr url action method 
         url-request-data url-request-method orig-url )
    (message "clicked: %s : %S" 
             (car (widget-get parent 'ident))
             (widget-value parent))
    (setq name-value 
          (list (cons (car (widget-get parent 'ident)) (widget-value parent))))
    (mapc
     (lambda (widget)
       (let ((ident (widget-get widget 'ident)) type name value)
         (setq name (car ident) type (cdr ident))
         (when (and (not (member type '("submit" "reset" "button" "image")))
                    (setq value (widget-value widget)))
             (if (listp value) 
                 (mapc 
                  (lambda (v) 
                    (setq name-value (cons (cons name v) name-value))) value)
               (setq name-value (cons (cons name value) name-value)) )) )) 
     wiki-form-ui-data)
    (mapc 
     (lambda (nv)
       (setq encoded 
             (cons (concat (url-hexify-string (car nv)) "="
                           (url-hexify-string (cdr nv))) encoded)))
     name-value)
    (setq encoded (mapconcat 'identity (nreverse encoded) "&")
          encoded (replace-regexp-in-string "%20" "+" encoded)) 
    (setq f-attr (xml-node-attributes wiki-form-xml-data)
          action (cdr (assq 'action f-attr))
          method (cdr (assq 'method f-attr)))
    (if (member method '("GET" "get" nil))
        (setq url (url-generic-parse-url (concat action "?" encoded)))
      (setq url                       (url-generic-parse-url action)
            url-request-method        (upcase method)
            url-request-data          encoded            
            url-request-extra-headers 
            '(("Content-Type" . "application/x-www-form-urlencoded")) ))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; fill in the missing bits of the action url if need be:
    (when (setq orig-url (cdr (assq :url wiki-edit-data)))
      (setq orig-url (url-generic-parse-url orig-url))
      (if (< (length (url-host url)) 1) (url-set-host url (url-host orig-url)))
      (if (< (length (url-type url)) 1) (url-set-type url (url-type orig-url)))
      (if (or (integerp (url-port url)) 
              (< (length (url-port url)) 1))
          (url-set-port url (url-port orig-url))) )
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; ok, we're good to go:
    (let ((rbuf (url-retrieve-synchronously url)) rxhtml callback body fbuf)
      (setq callback (cdr (assq :submit wiki-edit-data))
            fbuf     (current-buffer))
      (set-buffer rbuf)
      (if callback
          ;; we were given a callback, if it says we're done (true return)
          ;; then nuke everything related to the form, we are out of here:
          (when (funcall callback) (kill-buffer rbuf) (kill-buffer fbuf))
        ;; otherwise display the text of the response page:
        (setq rxhtml (car (xml-parse-region (point-min) (point-max))))
        (mapc
         (lambda (node)
           (when (and (listp node) (eq (xml-node-name node) 'body))
             (setq body node)))
         (xml-node-children rxhtml))
        (when body 
          (kill-buffer fbuf)
          (message 
           "%s" (replace-regexp-in-string
                 "\\(^\\s-*\n\\)+" "" (wiki-xml-node-text body)))))
      (kill-buffer rbuf)) ))

(defun wiki-form-ui-reset (&optional parent child event &rest stuff)
  "Reset the emacs widgets representing the xhtml form in the current buffer
to the values in the original xhtml represented by `wiki-form-xml-data'."
  (when wiki-form-xml-data
    (wiki-form-xml-data-to-ui wiki-form-xml-data (current-buffer))))

(provide 'wiki-form)
