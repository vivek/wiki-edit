;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; oddmuse wiki support
(defconst wiki-oddmuse-xlink-regex
  (concat "\\(?:^\\|[^!]\\)"
          "\\<\\([a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\)\\>\\|"
          "\\[\\[\\(.+?\\)\\]\\]"))

(defun wiki-oddmuse-target-at-point (point)
  "Return the string surrounding POINT if it is a wiki link, or nil."
  (save-excursion
    (let ((end nil) (x t) (target nil) (case-fold-search nil))
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      ;; search forwards till we find an xref that spans point:
      (while (and x (search-forward-regexp wiki-oddmuse-xlink-regex end t))
        (and (<= (or (match-beginning 1) (1- (match-beginning 2))) point)
             (>= (or (match-end       1)     (match-end       2) ) point)
             (setq x nil target (or (match-string 1) (match-string 2)))))
      target) ))

(defun wiki-oddmuse-fix-bogus-xhtml () 
  (save-excursion
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; malformed hr and br tags
    (beginning-of-buffer)
    (while (re-search-forward "<\\([hb]r\\>[^/>]*\\)>" nil t)
      (replace-match  "<\\1/>" nil nil)) ))

(defun wiki-oddmuse-form-ok-p (form)
  (let ((ok nil))
    (mapc 
     (lambda (e) 
       (let ((attr (xml-node-attributes e))) 
         (when (equal (cdr (assq 'name attr)) "Save") (setq ok t))))
     (xml-node-children form)) ok))

(defun wiki-oddmuse-choose-form (forms)
  (let ((form nil)) 
    (mapc (lambda (F) 
            (if (wiki-oddmuse-form-ok-p F) (setq form F))) forms) form))

(defconst wiki-oddmuse-ignore '("Cancel" "Preview"))
(defun wiki-oddmuse-form-element-value (element)
  (let ((attr (xml-node-attributes element)))
    (when (and (not (eq 'textarea (xml-node-name element)))
               (not (member (cdr (assq 'name attr)) wiki-oddmuse-ignore)))
      
      (if (equal (cdr (assq 'name (xml-node-attributes element))) "answer")
          (cons "answer" "emacs")
        (wiki-xml-form-element-value element))
      ) ))

(defun wiki-oddmuse-form-values (form)
  (delete nil 
          (mapcar 'wiki-oddmuse-form-element-value (xml-node-children form)) ))

(defun wiki-oddmuse-page-title (form xhtml)
  (let ((title nil))
    (mapc 
     (lambda (e) 
       (let ((attr (xml-node-attributes e))) 
         (when (equal (cdr (assq 'name attr)) "title") 
           (setq title (cdr (assq 'value attr)))))) 
     (xml-node-children form)) 
    title))

(defun wiki-oddmuse-buffer-name (form values title)
  (format "*wiki-oddmuse: %s [%s]*"
          (funcall (wiki-fun 'fix-string) title) 
          (cdr (assoc "oldtime" values))) )

(defvar wiki-oddmuse-messages nil)
(defun wiki-oddmuse-extract-messages (xml)
  (let ((wiki-oddmuse-messages nil))
    (mapc 'wiki-oddmuse-extract-messages-i xml) 
    wiki-oddmuse-messages))
(defun wiki-oddmuse-extract-messages-i (xml-node)
  (if (stringp xml-node) 
      nil 
    (let ((tag  (xml-node-name       xml-node))
          (attr (xml-node-attributes xml-node)))
      (if (and (eq tag 'div) (equal (cdr (assq 'class attr)) "message"))
          (setq wiki-oddmuse-messages 
                (cons xml-node wiki-oddmuse-messages)) 
        (mapc 'wiki-oddmuse-extract-messages-i 
              (xml-node-children xml-node)) )) ))

(defun wiki-oddmuse-display-response (&optional status-list page buffer)
  "Check for a collision and re-edit if there was one, otherwise say we're Ok."
  ;;(message "args: %S %S %S" page status-list buffer)
  (save-excursion
    (let ((start  (point-min))
          (re-editing nil)
          (msgs       nil)
          (xhtml      nil))
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ;; look for message we understand:
      (wiki-fun 'fix-bogus-xhtml)
      (setq xhtml (xml-parse-region (point-min) (point-max))
            msgs  (wiki-oddmuse-extract-messages xhtml))
      
      (mapc (lambda (m) 
              (and (string-match "\\<conflict\\>" m) (setq re-editing t))) msgs)
      (when msgs 
        (message "%s" (mapconcat 'identity msgs "\n"))
        (sit-for 1))

      ;; if we collided, rename the old buffer instead of killing it:
      (if (bufferp buffer)
          (if re-editing
              (let ((cbuf (current-buffer)))
                (set-buffer buffer)
                (rename-buffer
                 (replace-regexp-in-string "\\[.+\\]"
                                           "[OLD]" (buffer-name)))
                (set-buffer cbuf)
                (wiki-edit page))
            (message "killing oddmuse buffer: %S" buffer)
            (kill-buffer buffer) )) ))
  ;; this kills the http data buffer, may not be necessary now that
  ;; url-http-parse-headers is fixed below:
  (kill-buffer (current-buffer) ))

(defun wiki-oddmuse-edit-url (wiki page) 
  (let ( (target (url-hexify-string page))
         (base   (cdr (assq :base-url (assoc wiki wiki-known-wikis)))) )
    (concat base "?action=edit;id=" target)))

(defvar wiki-oddmuse-mode-keywords
  '( ("^`[^']+'" (0 'mode-line t))
     ("\\(?:^\\|[^!]\\)\\<[A-Z][a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\>"
      (0 font-variable-name-face t))
     ("\\(\\*\\)\\([^ \t\n\r0-9/~_#*].*?\\)\\(\\*\\)"
      (1 font-lock-builtin-face t)
      (2 'bold prepend)
      (3 font-lock-builtin-face t))
     ("\\(/\\)\\([^ \t\n\r0-9/~_#*].*?\\)\\(/\\)"
      (1 font-lock-builtin-face t)
      (2 'italic prepend)
      (3 font-lock-builtin-face t))
     ("\\(~\\)\\([^ \t\n\r0-9/~_#*].*?\\)\\(~\\)"
      (1 font-lock-builtin-face t)
      (2 'highlight prepend)
      (3 font-lock-builtin-face t))
     ("\\(_\\)\\([^ \t\n\r0-9/~_#*].*?\\)\\(_\\)"
      (1 font-lock-builtin-face t)
      (2 'underline prepend)
      (3 font-lock-builtin-face t))
     ("\\(\\*\\*\\)\\(.*?\\)\\(\\*\\*\\)"
      (1 font-lock-builtin-face t)
      (2 'bold prepend)
      (3 font-lock-builtin-face t))
     ("\\(//\\)\\(.*?\\)\\(//\\)"
      (1 font-lock-builtin-face t)
      (2 'italic prepend)
      (3 font-lock-builtin-face t))
     ("\\(~~\\)\\(.*?\\)\\(~~\\)"
      (1 font-lock-builtin-face t)
      (2 'highlight prepend)
      (3 font-lock-builtin-face t))
     ("\\(__\\)\\(.*?\\)\\(__\\)"
      (1 font-lock-builtin-face t)
      (2 'underline prepend)
      (3 font-lock-builtin-face t))
     ("\\(##\\)\\(.*?\\)\\(##\\)"
      (1 font-lock-builtin-face t)
      (2 'fixed-pitch prepend)
      (3 font-lock-builtin-face t))
     ("\\('''''\\|'''\\|''\\)\\(.*?\\)\\(\\1\\)"
      (1 font-lock-builtin-face t)
      (2 (let ( (markup (length (match-string 1))) )
           (cond ((eq markup 5) 'bold-italic)
                 ((eq markup 3) 'bold       )
                 ((eq markup 2) 'italic     ))) prepend)
      (3 font-lock-builtin-face t))
     ("^\\(#+\\|\\*+\\|:+\\) " (0 font-lock-keyword-face t))
     ("^\\(;+\\) \\(.*?\\):" 
      (1 font-lock-keyword-face t)
      (2 font-lock-variable-name-face prepend))
     ("^\\s-*\\(=\\{1,5\\}\\)\\(.*?\\)\\(\\1\\)\\(?:[^=]\\|$\\)"
      (1 font-lock-builtin-face t)
      (2 (intern-soft (format "wiki-head-%d" (length (match-string 1)))) append)
      (3 font-lock-builtin-face t))
     ("^\\([ \t\r]+\\)\\([^=].*[^=]\\)\\s-*$" 
      (0 'highlight nil) 
      (1 fixed-pitch prepend))
     ("\\(\\[\\[\\)\\(image:\\|\\)\\([^[ \t].+?\\)\\(\\]\\]\\)"
      (1 font-lock-builtin-face t)
      (2 font-lock-function-name-face t)
      (3 font-lock-keyword-face t)
      (4 font-lock-builtin-face t))
     ("\\(?:^\\|[^[]\\)\\(\\[\\)\\([^[].*\\)\\(\\]\\)\\(?:[^]]\\|$\\)"
      (1 font-lock-builtin-face t)
      (2 font-lock-variable-name-face t)
      (3 font-lock-builtin-face t))
     ("\\(?:[^-]\\|^\\)----\\(?:[^-]\\|$\\)" . font-lock-builtin-face)
     )
  "The `wiki-oddmuse-mode' setting for `font-lock-defaults.'")

(defvar wiki-oddmuse-mode-syntactic-keywords
  '(("{{{"       (0 "|" keep))    ;; generic string fence 1
    ("}}}"       (0 "|" keep))    ;; generic string fence 2
    ("%%"        (0 "!" t   ))
    ) ) ;; generic comment finish

(wiki-define-mode 'oddmuse)

(provide 'wiki-oddmuse)
