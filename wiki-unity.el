;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; unity wiki support
(defconst wiki-unity-xlink-regex
  "\\<\\([a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\)\\>\\|\\[\\(.+?\\)\\]")

(defun wiki-unity-target-at-point (point)
  "Return the string surrounding POINT if it is a wiki link, or nil."
  (save-excursion
    (let ((end nil) (x t) (target nil) (case-fold-search nil))
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      ;; search forwards till we find an xref that spans point:
      (while (and x (search-forward-regexp wiki-unity-xlink-regex end t))
        (and (<= (or (match-beginning 1) (1- (match-beginning 2))) point)
             (>= (or (match-end       1)     (match-end       2) ) point)
             (setq x nil target (or (match-string 1) (match-string 2)))))
      ;; make sure we don't match [[macros]]
      (if (and target (eq (aref target 0) ?\[)) nil target)) ))

;;(defun wiki-unity-fix-bogus-xhtml () t)
(defun wiki-unity-fix-bogus-xhtml ()
  (save-excursion
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; under-encoded wiki markup in textareas:
    (beginning-of-buffer)
    (while (re-search-forward 
            (concat "<textarea\\(?:.\\|\n\\)*?>"
                    "\\(\\(?:.\\|\n\\)*?\\)"
                    "</textarea>") nil t)
      (let ((text (match-string 1))) 
        (save-match-data (setq text (wiki-text-to-html text))) 
        (replace-match text nil nil nil 1)))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; missing <html> tag
    (beginning-of-buffer)
    (when (not (re-search-forward "<html" nil t))
      (beginning-of-buffer)
      (message "inserting html tag")
      (when (re-search-forward "<head\\>" nil t)
        (replace-match "<html>\n<head" nil nil)))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; malformed hr and br tags
    (beginning-of-buffer)
    (while (re-search-forward "<\\([hb]r\\>[^/>]*\\)>" nil t)
      (replace-match  "<\\1/>" nil nil))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; malformed <link/> tags
    (message "fixing <link/meta/input ...> tags")
    (beginning-of-buffer)
    (while (re-search-forward 
            (concat "<\\(\\(?:link\\|meta\\|input\\|img\\)\\>"
                    "\\(?:.\\|\n\\)*?\\)"
                    "\\(.\\)>"           ) nil t)
      (when (not (string= "/" (match-string 2)))
        (message "found broken tag '%s'" (match-string 0))
        (replace-match  "<\\1\\2/>" nil nil)))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; p tags. just generally fucked up all over the place.
    (beginning-of-buffer)
    (while (re-search-forward 
            (concat "<p\\>.*?>\\|"
                    "</\\(?:p\\|em\\|strong\\)>\\|"
                    "<strong\\>.*?>\\|"
                    "<em\\>.*?>"                   ) nil t)
      (message "found broken? tag '%s'" (match-string 0))
      (sit-for 0)
      (replace-match ""))
    (message "done eliding up incorrigible tags")
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; unquoted input type attributes and/or missing / in tag:
    ;;(beginning-of-buffer)
    ;;(while (re-search-forward 
    ;;      "\\(<input\\>.*\\<type=\\)\"?\\(\\S-+?\\>\\)\"?\\(.*?\\)\\(/?\\)>" 
    ;;        nil t)
    ;;  (replace-match  "\\1\"\\2\"\\3/>" nil nil)) 
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; missing quotes around atributes
    ;;(beginning-of-buffer)
    ;;(while (re-search-forward "<\\S-+ \\([^>]+\\)>" nil t)
    ;;  (let ((end (match-end 1)))
    ;;    (goto-char (match-beginning 1))
    ;;    (while (re-search-forward "\\(\\<\\S-+\\>=\\)'\\([^']*\\)'" end t)
    ;;      (replace-match "\\1\"\\2\"")) ))
    (end-of-buffer)
    ;;(insert "</body></html>") 
    ))

(defun wiki-unity-form-ok-p (form)
  (let ((ok nil))
    (mapc 
     (lambda (e) 
       (let ((attr (xml-node-attributes e))) 
         (when (equal (cdr (assq 'name attr)) "savetext") (setq ok t))))
     (xml-node-children form)) ok))

(defun wiki-unity-choose-form (forms)
  (let ((form nil)) 
    (mapc (lambda (F) (if (wiki-unity-form-ok-p F) (setq form F))) forms) form))

(defun wiki-unity-page-title (form xhtml)
  (let (title action)
    (setq action (cdr (assq 'action (xml-node-attributes form))))
    (when (stringp action)
      (when (string-match "/\\([^#]+\\)" action) 
        (setq title (match-string 1 action)) ))
    (when (not title)
      (error "Page title not found: Wiki xhtml form probably changed"))
    title))

(defun wiki-unity-buffer-name (form values title)
  (format "*wiki-unity: %s [%s]*"
          (funcall (wiki-fun 'fix-string) title) 
          (cdr (assoc "ts" values))) )

(defconst wiki-unity-response-ok-fmt
  "</table>\\(\\(?:.\\|\n\\)*?\\)<a [^>]*name\\s-*=\\s-*\"%s\"")

(defun wiki-unity-display-response (&optional status-list page buffer)
  "If possible, fillet the Unity response and display it in the message area.
Otherwise show the whole response."
  ;;(message "args: %S %S %S" page status-list buffer)
  (save-excursion
    (let ((start  (point-min))
          (re-editing nil)
          (regex  (format wiki-unity-response-ok-fmt page))
          (regex2 "<body\\(?:.\\|\n\\)+\\(Re-edit: \\(?:.\\|\n\\)*?\\)<form"))
      ;; look for message we understand:
      (cond
       ;; the Ok/accepted message:
       ( (progn (goto-char start) (search-forward-regexp regex  nil :no-error))
         (message "%s" (wiki-strip-tags (match-string 1))) )
       ;; whoops, we collided:
       ( (progn (goto-char start) (search-forward-regexp regex2 nil :no-error))
         (setq re-editing t)
         (message
          "%s" (wiki-compact-string (wiki-strip-tags (match-string 1))))
         (sit-for 0)
         (set-buffer buffer)
         (wiki-edit page) )
       ;; arse. something unexpected:
       ( t (display-buffer (current-buffer)) ) )

      ;; if we collided, rename the old buffer instead of killing it:
      (if (bufferp buffer)
          (if re-editing
              (let ((cbuf (current-buffer)))
                (set-buffer buffer)
                (rename-buffer
                 (replace-regexp-in-string "\\[.+\\]"
                                           "[OLD]" (buffer-name)))
                (set-buffer cbuf))
            (message "killing unity buffer: %S" buffer)
            (kill-buffer buffer) )) ))
  ;; this kills the http data buffer, may not be necessary now that
  ;; url-http-parse-headers is fixed below:
  (kill-buffer (current-buffer) ))

(defun wiki-unity-edit-url (wiki page) 
  (let ( (target (url-hexify-string page))
         (base   (cdr (assq :base-url (assoc wiki wiki-known-wikis)))) )
    (concat base target "?action=edit;editor=text")))

(defvar wiki-unity-mode-keywords
  '( ("^<[^<>]+>" (0 'mode-line t))
     ("\\<[A-Z][a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\>" . font-lock-keyword-face)
     ("\\(\\[\\)\\([^[ \t].+?\\)\\(\\]\\)"
      (1 font-lock-builtin-face t)
      (2 font-lock-keyword-face t)
      (3 font-lock-builtin-face t))
     ("\\(?:^\\|[^']\\)\\(''\\)\\([^'].*?\\)\\(''\\)\\(?:[^']\\|$\\)"
      (1 font-lock-builtin-face t)
      (2 'italic prepend)
      (3 font-lock-builtin-face t))
     ("\\('''\\)\\(.*?\\)\\('''\\)"
      (1 font-lock-builtin-face t)
      (2 'bold prepend)
      (3 font-lock-builtin-face t))
     ("\\(###\\)\\([^[ \t\n]*\\)"
      (1 font-lock-builtin-face  t)
      (2 font-lock-constant-face t))
     ("||"             (0 font-lock-string-face   t))
     ("\\(||\\)\\s-*$" (1 font-lock-builtin-face  t))
     ("^\\s-*\\(||\\)" (1 font-lock-builtin-face  t))
     ("\\(\\[\\[\\)\\(.+?\\)\\(\\]\\]\\)"
      (1 font-lock-builtin-face       t)
      (2 font-lock-function-name-face t)
      (3 font-lock-builtin-face       t))
     ("\\(?:^\\|[^#]\\)\\(##\\)\\([^[#][^ \t\n]+\\|\\[.+?\\]\\)"
      (1 font-lock-builtin-face  t)
      (2 'underline              append))
     ("\\(?:[^-]\\|^\\)----\\(?:[^-]\\|$\\)" . font-lock-builtin-face)
     ("<<<\\(?:.\\|\n\\)+?>>>"
      (0 font-lock-comment-face append))
     )
  "The `wiki-unity-mode' setting for `font-lock-defaults.'")

(defvar wiki-unity-mode-syntactic-keywords
  '(("{{{"       (0 "|" keep))    ;; generic string fence 1
    ("}}}"       (0 "|" keep))    ;; generic string fence 2
    ("<<<"       (0 "<" keep))    ;; generic comment start
    (">>\\(>\\)" (1 ">" keep))) ) ;; generic comment finish

(wiki-define-mode 'unity)

(provide 'wiki-unity)
