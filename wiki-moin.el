;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; moin moin wiki support
(defun wiki-moin-edit-url (wiki page) 
  (let ( (target (url-hexify-string page))
         (base   (cdr (assq :base-url (assoc wiki wiki-known-wikis)))) )
    (concat base target "?action=edit;editor=text")))

(defun wiki-moin-fix-bogus-xhtml (&optional tmp) 
  (save-excursion
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; malformed meta and link tags
    (beginning-of-buffer)
    (while (re-search-forward
            "<\\(\\(?:img\\|input\\|link\\|meta\\)[^>]*\\)\\(.\\)>" nil t)
      (if (not (equal (match-string 2) "/"))
          (replace-match  "<\\1\\2/>" nil nil)))
    ;; p tags horribly borked
    (beginning-of-buffer)
    (while (re-search-forward "</?\\(?:p\\|div\\)\\(?:\\s-+.+?>\\|>\\)" nil t)
      (replace-match  "" nil nil))
    (beginning-of-buffer)
    (while (re-search-forward "<\\S-+ \\([^>]+\\)/?>" nil t)
      (let ((end (match-end 1)))
        (save-restriction
          (message "narrowing to %d - %d" (match-beginning 1) end)
          (narrow-to-region (match-beginning 1) end)
          (goto-char (point-min))
          (while (re-search-forward
                  (concat "\\(\\S-+?=\\)\\s-*"
                          "\\(?:"
                          "\\([\"']\\)" "\\(?:.\\|\n\\)+?" "\\2"
                          "\\|"
                          "\\([^\"']\\S-*\\)"
                          "\\)")
                  nil t)
            (if (match-string 3)
                (replace-match "\\1\"\\3\"")) )) )) ))

(wiki-define-mode 'moin)

(provide 'wiki-moin)
