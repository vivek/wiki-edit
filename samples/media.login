<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex,nofollow" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html" />
		<title>Log in / create account - Narf</title>
		<style type="text/css" media="screen,projection">/*<![CDATA[*/ @import "/mediawiki/skins/monobook/main.css?9"; /*]]>*/</style>
		<link rel="stylesheet" type="text/css" media="print" href="/mediawiki/skins/common/commonPrint.css" />

		<!--[if lt IE 5.5000]><style type="text/css">@import "/mediawiki/skins/monobook/IE50Fixes.css";</style><![endif]-->
		<!--[if IE 5.5000]><style type="text/css">@import "/mediawiki/skins/monobook/IE55Fixes.css";</style><![endif]-->
		<!--[if IE 6]><style type="text/css">@import "/mediawiki/skins/monobook/IE60Fixes.css";</style><![endif]-->
		<!--[if IE 7]><style type="text/css">@import "/mediawiki/skins/monobook/IE70Fixes.css?1";</style><![endif]-->
		<!--[if lt IE 7]><script type="text/javascript" src="/mediawiki/skins/common/IEFixes.js"></script>
		<meta http-equiv="imagetoolbar" content="no" /><![endif]-->
		<script type="text/javascript">var skin = 'monobook';var stylepath = '/mediawiki/skins';</script>
		<script type="text/javascript" src="/mediawiki/skins/common/wikibits.js?1"><!-- wikibits js --></script>
		<script type="text/javascript" src="/mediawiki/index.php?title=-&amp;action=raw&amp;gen=js"><!-- site js --></script>

		<style type="text/css">/*<![CDATA[*/
@import "/mediawiki/index.php?title=MediaWiki:Common.css&action=raw&ctype=text/css&smaxage=18000";
@import "/mediawiki/index.php?title=MediaWiki:Monobook.css&action=raw&ctype=text/css&smaxage=18000";
@import "/mediawiki/index.php?title=-&action=raw&gen=css&maxage=18000";
/*]]>*/</style>
		<!-- Head Scripts -->
			</head>
<body  class="ns--1 ltr">
	<div id="globalWrapper">
		<div id="column-content">
	<div id="content">
		<a name="top" id="top"></a>
				<h1 class="firstHeading">Log in / create account</h1>

		<div id="bodyContent">
			<h3 id="siteSub">From Narf</h3>
			<div id="contentSub"></div>
									<div id="jump-to-nav">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div>			<!-- start content -->
			
<div id="userloginForm">
<form name="userlogin" method="post" action="/mediawiki/index.php?title=Special:Userlogin&amp;action=submitlogin&amp;type=login&amp;returnto=Main_Page">

	<h2>Log in</h2>
	<p id="userloginlink">Don't have a login? <a href="/mediawiki/index.php?title=Special:Userlogin&amp;type=signup&amp;returnto=Main_Page">Create an account</a>.</p>
	<div id="userloginprompt"><p>You must have cookies enabled to log in to Narf.
</p></div>
		<table>
		<tr>
			<td align='right'><label for='wpName1'>Username:</label></td>

			<td align='left'>
				<input type='text' class='loginText' name="wpName" id="wpName1"
					value="Vivek@etla.org" size='20' />
			</td>
		</tr>
		<tr>
			<td align='right'><label for='wpPassword1'>Password:</label></td>
			<td align='left'>
				<input type='password' class='loginPassword' name="wpPassword" id="wpPassword1"
					value="" size='20' />

			</td>
		</tr>
			<tr>
			<td></td>
			<td align='left'>
				<input type='checkbox' name="wpRemember"
					value="1" id="wpRemember"
										/> <label for="wpRemember">Remember me</label>
			</td>
		</tr>

		<tr>
			<td></td>
			<td align='left' style="white-space:nowrap">
				<input type='submit' name="wpLoginattempt" id="wpLoginattempt" value="Log in" />&nbsp;			</td>
		</tr>
	</table>
</form>
</div>
<div id="loginend"></div>
<div class="printfooter">

Retrieved from "<a href="http://localhost/mediawiki/index.php/Special:Userlogin">http://localhost/mediawiki/index.php/Special:Userlogin</a>"</div>
						<!-- end content -->
			<div class="visualClear"></div>
		</div>
	</div>
		</div>
		<div id="column-one">
	<div id="p-cactions" class="portlet">

		<h5>Views</h5>
		<ul>
				 <li id="ca-article" class="selected"><a href="/mediawiki/index.php?title=Special:Userlogin&amp;returnto=Main_Page">Special Page</a></li>
		</ul>
	</div>
	<div class="portlet" id="p-personal">
		<h5>Personal tools</h5>

		<div class="pBody">
			<ul>
				<li id="pt-anonuserpage"><a href="/mediawiki/index.php/User:127.0.0.1" class="new">127.0.0.1</a></li>
				<li id="pt-anontalk"><a href="/mediawiki/index.php/User_talk:127.0.0.1" class="new">Talk for this IP</a></li>
				<li id="pt-anonlogin" class="active"><a href="/mediawiki/index.php?title=Special:Userlogin&amp;returnto=Special:Userlogin">Log in / create account</a></li>
			</ul>
		</div>

	</div>
	<div class="portlet" id="p-logo">
		<a style="background-image: url(/mediawiki/skins/common/images/wiki.png);" href="/mediawiki/index.php/Main_Page" title="Main Page"></a>
	</div>
	<script type="text/javascript"> if (window.isMSIE55) fixalpha(); </script>
		<div class='portlet' id='p-navigation'>
		<h5>Navigation</h5>
		<div class='pBody'>

			<ul>
				<li id="n-mainpage"><a href="/mediawiki/index.php/Main_Page">Main Page</a></li>
				<li id="n-portal"><a href="/mediawiki/index.php/Narf:Community_Portal">Community portal</a></li>
				<li id="n-currentevents"><a href="/mediawiki/index.php/Current_events">Current events</a></li>
				<li id="n-recentchanges"><a href="/mediawiki/index.php/Special:Recentchanges">Recent changes</a></li>
				<li id="n-randompage"><a href="/mediawiki/index.php/Special:Random">Random page</a></li>

				<li id="n-help"><a href="/mediawiki/index.php/Help:Contents">Help</a></li>
				<li id="n-sitesupport"><a href="/mediawiki/index.php/Narf:Site_support">Donations</a></li>
			</ul>
		</div>
	</div>
		<div id="p-search" class="portlet">
		<h5><label for="searchInput">Search</label></h5>

		<div id="searchBody" class="pBody">
			<form action="/mediawiki/index.php/Special:Search" id="searchform"><div>
				<input id="searchInput" name="search" type="text" accesskey="f" value="" />
				<input type='submit' name="go" class="searchButton" id="searchGoButton"	value="Go" />&nbsp;
				<input type='submit' name="fulltext" class="searchButton" value="Search" />
			</div></form>
		</div>
	</div>
	<div class="portlet" id="p-tb">

		<h5>Toolbox</h5>
		<div class="pBody">
			<ul>
<li id="t-specialpages"><a href="/mediawiki/index.php/Special:Specialpages">Special pages</a></li>
			</ul>
		</div>
	</div>
		</div><!-- end of the left (by default at least) column -->

			<div class="visualClear"></div>
			<div id="footer">
				<div id="f-poweredbyico"><a href="http://www.mediawiki.org/"><img src="/mediawiki/skins/common/images/poweredby_mediawiki_88x31.png" alt="MediaWiki" /></a></div>
				<div id="f-copyrightico"><a href="http://www.gnu.org/copyleft/fdl.html"><img src="/mediawiki/skins/common/images/gnu-fdl.png" alt='GNU Free Documentation License 1.2' /></a></div>
			<ul id="f-list">
				<li id="privacy"><a href="/mediawiki/index.php/Narf:Privacy_policy" title="Narf:Privacy policy">Privacy policy</a></li>
				<li id="about"><a href="/mediawiki/index.php/Narf:About" title="Narf:About">About Narf</a></li>
				<li id="disclaimer"><a href="/mediawiki/index.php/Narf:General_disclaimer" title="Narf:General disclaimer">Disclaimers</a></li>

			</ul>
		</div>
	<script type="text/javascript"> if (window.runOnloadHook) runOnloadHook();</script>
</div>
<!-- Served by nerf in 0.185 secs. -->
</body></html>
