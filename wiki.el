;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(defconst wiki-version 0.11 "version of wiki")
(when load-file-name
  (add-to-list 'load-path (file-name-directory load-file-name))
  (when (and (boundp 'find-function-source-path) find-function-source-path)
    (add-to-list 'find-function-source-path 
                 (file-name-directory load-file-name))) )
(require 'wiki-+)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; elisp-dep-block >>
(require 'diff)      ;;(diff)
(require 'ediff)     ;;(ediff-buffers)
(require 'time-date) ;;(seconds-to-time)
(require 'url)       ;;(url-retrieve)
(require 'url-auth)  ;;(url-register-auth-scheme)
(require 'url-parse) ;;(url-generic-parse-url)
(require 'url-util)  ;;(url-unhex-string url-hexify-string)
;; elisp-dep-block <<
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun url-set-filename (url value) (setf (url-filename url) value))
(defun url-set-host     (url value) (setf (url-host     url) value))
(defun url-set-port     (url value) (setf (url-port     url) value))
(defun url-set-type     (url value) (setf (url-type     url) value))
(defun url-set-user     (url value) (setf (url-user     url) value))
(defun url-set-password (url value) (setf (url-password url) value))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki configuration items and special widgets:
;; these two mostly exist because the layout of the standard widgets 
;; is somewhat less than desirable:
(define-widget 'wiki-cons 'cons
  "A key-value alist with a fixed set of keys"
  :tag    ""
  :format "%v")

(define-widget 'wiki-const 'const "Invisible constant" :format "")

(defcustom wiki-known-wikis
  '(("wikipaedia"
     (:type     . media)
     (:base-url . "http://en.wikipedia.org/w/index.php")
     (:username . ""))
    ("emacs"
     (:type     . oddmuse)
     (:base-url . "http://www.emacswiki.org/cgi-bin/wiki/")
     (:username . "")))
  "A list of known wikis and their configuration"
  :group 'wiki
  :type  '(repeat
           (group :tag "Wiki Configuration" 
                  (string :tag "Name")
                  (wiki-cons (wiki-const :type)
                             (choice :tag "Type    "
                                     (const :tag "Trac Wiki " trac   )
                                     (const :tag "Unity Wiki" unity  )
                                     (const :tag "Media Wiki" media  )
                                     (const :tag "Moin Moin " moin   )
                                     (const :tag "Oddmuse   " oddmuse)
                                     (const :tag "Usemod    " usemod )))
                  (wiki-cons (wiki-const :base-url)
                             (string     :tag "Base URL" :value ""))
                  (wiki-cons (wiki-const :username)
                             (string     :tag "Username" :value "")) )) )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; magic to fill in the username automatically when fetching a wiki page
(defadvice read-string (around wiki-ui activate)
  "A little hackery to make read-string default to the current wiki's 
recorded username when fetching a page to edit."
  (let ((p                 (ad-get-arg 0)) 
        (flag              wiki-fetching-url)
        (case-fold-search  t)
        (wiki-fetching-url nil)
        (name              nil))    
    (when (and flag (string-match "\\<user\\(?:name\\)?\\>" p))
      ;;(message "fetching username from %S" wiki-current-name)
      (setq name 
            (or (cdr (assq :username 
                           (cdr (assoc wiki-current-name wiki-known-wikis)))) 
                (user-login-name)) )
      (ad-set-arg 1 name)
      (ad-set-arg 3 name))
    ad-do-it))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar wiki-fetching-url nil)
(defvar wiki-current-name nil)

(defvar wiki-edit-data nil
  "An alist of alists: the top level keys are the classes of data
which are associated with an alist of name/value pairs for that class of item.")
(make-variable-buffer-local 'wiki-edit-data)
(put 'wiki-edit-data 'permanent-local t)

(defvar wiki-flavour nil "A symbol specifying the wiki flavour in effect.")
(make-variable-buffer-local 'wiki-flavour)
(put 'wiki-flavour 'permanent-local t)

(defvar wiki-name nil "The name of the wiki we are using (a string).")
(make-variable-buffer-local 'wiki-name)
(put 'wiki-name 'permanent-local t)

(defvar wiki-page-url nil "URL for this wiki page")
(make-variable-buffer-local 'wiki-page-url)
(put 'wiki-page-url 'permanent-local t)

(defvar wiki-twin-buffers nil
  "A list of buffers that should be killed when this buffer is killed.")
(make-variable-buffer-local 'wiki-twin-buffers)
(put 'wiki-twin-buffers 'permanent-local t)

(defcustom wiki-electric-comments nil
  "Whether to use the magic comment insertion function bound to \"<\""
  :type '(boolean))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; statistics
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-mean (numbers)
  "Return the mean of NUMBERS (a list). For convenience, an empty list
will elicit a response of 0."
  (if numbers (/ (apply '+ (mapcar 'float numbers)) (length numbers)) 0))

(defun wiki-sample-variance (values)
  "Calculate the \(bias corrected\) sample variance for VALUES.
For reasons of convenience, we will return 0 for empty lists or lists
consisting of 1 item."
  (let ( (mu (wiki-mean values))
	 (j  (float (- (length values) 1))) )
    (if (memq (round j) '(0 -1)) 0 ;; unsafe operation, return 0
      (/ (apply '+
		(mapcar (lambda (N) (expt (- (float N) mu) 2)) values)) j)) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; string munging
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-query-encode (string-list)
  "Encode each element of STRING-LIST in the customary CGI encoding: 
spaces are encoded as '+', anything not alphanumeric, '.' '/' or '-' is encoded
as its %XX byte-as-hex code. Each string is encoded as utf-8 before CGI
ancoding, so unicode %XXXX escapes need not be considered here."
  (mapcar
   (lambda (S)
     (or (stringp S) (setq S (format "%s" S)))
     (replace-regexp-in-string
      "[^A-Za-z_0-9.\/-]"
      (lambda (C) (if (equal C " ") "+" (format "%%%02x" (string-to-char C))))
      (encode-coding-string S 'utf-8 t) :fixed-case :literal)) string-list))

(defun wiki-strip-tags (string)
  "Strip <> tags from a copy of STRING and return it."
  (replace-regexp-in-string "<[^>]+>" "" string))

(defun wiki-compact-string (string)
  "Strip all whitespace from the beginning of a string."
  (replace-regexp-in-string "^\\s-+" "" string))

;;(defun wiki-strip-markup (string)
;;  "Strip some markup from STRING. "
;;  (replace-regexp-in-string "'''?\\|[][]" "" string))

(defun wiki-trim-whitespace (string)
  "Return a copy of STRING with leading and trailing whitespace removed."
  (replace-regexp-in-string "^\\s-*\\|\\s-*$" "" string))

(defun wiki-join (sep list)
  (mapconcat 'identity list sep))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki edit buffer widget creation/destruction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defconst wiki-sep-props
  (list 'rear-nonsticky t
	'read-only      t
	'fontified      t
	'intangible     t
	'face           'mode-line))

(defun wiki-text-section (text-cons)
  "Given a (name . value) cons TEXT-CONS, insert a wiki text edit section
into the current buffer."
  (let ((name (car text-cons))
	(data (cdr text-cons))
	(beg              nil)
	(end              nil)
	(len              nil))
    (setq beg (format "\n(%s) \n" (upcase name))
	  end (format "\n(/%s)\n" (upcase name))
	  len (length end))
    (if (= (length data) 0) (setq data " "))
    (put-text-property (- len 1) len 'wiki-text-beg name beg)
    (put-text-property 0         1   'wiki-text-end name end)
    (mapcar 
     (lambda (str)
       (add-text-properties 0 len wiki-sep-props str)
       (put-text-property 0        1   'face nil str)
       (put-text-property (1- len) len 'face nil str))
     (list beg end))
    (insert beg data end) ))

(defun wiki-kill-related ()
  "Kill any secondary buffers that shouldn't exist once this buffer is gone.
Targets are the buffers in `wiki-twin-buffers'."
  (let ((B wiki-twin-buffers) (C (current-buffer)))
    (setq wiki-twin-buffers nil) ;; just in case of circular relationship.
    (while B
      (set-buffer  (car B))
      (bury-buffer)
      (kill-buffer (car B))
      (setq    B   (cdr B)))
    (set-buffer C) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki and http buffer parsing (see also: table helper functions)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-parse-buffer ()
  "Grab the new (post-edit) savetext, graveyard etc values from the current
buffer, load them into a ((name .value) ...) alist and return it."
  (let ((beg  nil)
	(end  nil)
	(val  nil)
	(name nil)
	(data nil))
    (save-excursion
      (setq end (point-min))
      (while (setq beg (next-single-property-change end 'wiki-text-beg))
        (setq name (get-text-property beg 'wiki-text-beg)
              end  (next-single-property-change beg 'wiki-text-end)
              val  (buffer-substring-no-properties (+ 1 beg) end)
              val  (if (string-match "\\S-" val) val "")
              val  (if (eq (aref val 0) ?\n) (substring val 1) val)
              data (cons (cons name val) data)) ))
    data))

(defun wiki-zap-invisible ()
  "Remove text marked as \'invisible from the current buffer."
  (save-excursion
    (let ((x (point-min)) (y nil))
      (while (setq x (next-single-property-change x 'invisible))
	(when (get-text-property x 'invisible)
	  (setq y (or (next-single-property-change x 'invisible)
                      (point-max)))
	(delete-region x y) )) )) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; handle important entities
(defun wiki-entity-to-string (match)
  (format "%c" (string-to-int (match-string 1 match))))

(defun wiki-text-to-html (string)
  "Return a copy of STRING with \" < > and & converted into xhtml entities."
  (replace-regexp-in-string "\"" "&quot;"
   (replace-regexp-in-string "<" "&lt;"
    (replace-regexp-in-string ">" "&gt;"
     (replace-regexp-in-string "&" "&amp;" string) )) ))

(defun wiki-html-to-text (string)
  "Return a copy of STRING with the major named entities and numeric (byte)
entities converted into characters."
  (replace-regexp-in-string "&#\\([0-9]+\\);" 'wiki-entity-to-string
   (replace-regexp-in-string "&amp;" "&"
    (replace-regexp-in-string "&lt;" "<"
     (replace-regexp-in-string "&gt;" ">" string)) )) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; extract form values using xml.el
(defvar wiki-xml-form-data  nil)
(defvar wiki-xml-form-elems nil)
(defvar wiki-xml-form-radio nil)
(defvar wiki-xml-form-check nil)
(defvar wiki-xml-matching-children nil)

(defun wiki-xml-matching-children (parent condition tag)
  (let ((wiki-xml-matching-children nil))
    (wiki-xml-matching-children-1 parent condition tag)
    wiki-xml-matching-children))

(defun wiki-xml-matching-children-1 (parent condition tag)
  (cond 
   ((stringp parent) nil)                                 ;; char data
   ((or (and tag  (not (eq (xml-node-name parent) tag)))  ;; tag unmatched
        (and condition (not (funcall condition parent)))) ;; criteria not met
    (mapc (lambda (c) 
            (wiki-xml-matching-children-1 c condition tag)) 
          (xml-node-children parent)))
   (t
    (setq wiki-xml-matching-children 
          (cons parent wiki-xml-matching-children))) ))

(defun wiki-xml-node-text (xml-node)
  "Strip all tags from this xml.el style node structure and its descendents,
in other words, return the bare text contained within this node."
  (if (stringp xml-node) 
      ;;(progn (message "%s" xml-node) xml-node)
      xml-node
    (mapconcat 
     'identity 
     (mapcar 'wiki-xml-node-text (xml-node-children xml-node)) "\n") ))

(defun wiki-xml-form-data (xml-struct) 
  "Extract the form related nodes in the xml.el style XML-STRUCT, and return 
the xml.el style node-and-descendents structure constructed from them.
In other words, elide all non-form-related nodes and return the simplified
version of XML-STRUCT.
The top level elements of the returned structure will therefore correspond 
to <form> tags."
  (let ((wiki-xml-form-data  nil))
    (mapc 'wiki-xml-form-data-1 xml-struct) wiki-xml-form-data))
 
(defun wiki-xml-form-data-1 (xml-node)
  "Descend xml.el style structure XML-NODE, storing the descendents of each 
form in XML-NODE. Internal function used by `wiki-xml-form-data'."
  (if (stringp xml-node) nil 
    (if (eq (xml-node-name xml-node) 'form)
      (let ((wiki-xml-form-elems nil) 
            (wiki-xml-form-radio nil)
            (wiki-xml-form-check nil)
            (rbc-name            nil)
            (rbc-nodes           nil)
            (form-elems          nil)
            (form                nil))
        (mapc 'wiki-xml-form-elems (xml-node-children xml-node))
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;; retrieve the "scatterable" elements like checkbox and radio buttons
        ;; from their respective accumulators and gather them together
        (mapc 
         (lambda (node)
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
           (cond ((equal (car node) "radio")
                  (setq rbc-name  (cdr node)
                        rbc-nodes (cdr (assoc rbc-name wiki-xml-form-radio)))
                  (mapc (lambda (n)
                          (setq form-elems (cons n form-elems))) rbc-nodes))
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                 ((equal (car node) "checkbox")
                  (setq rbc-name  (cdr node)
                        rbc-nodes (cdr (assoc rbc-name wiki-xml-form-check)))
                  (mapc (lambda (n)
                          (setq form-elems (cons n form-elems))) rbc-nodes))
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                 (t (setq form-elems (cons node form-elems))) ))
         wiki-xml-form-elems)
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        (setq form 
              (cons 'form 
                    (cons (xml-node-attributes xml-node) form-elems)))
        ;;(message "adding %S to %S" form wiki-xml-form-data)
        (setq wiki-xml-form-data (cons form wiki-xml-form-data)) )
      (mapc 'wiki-xml-form-data-1 (xml-node-children xml-node)) ) ))

(defun wiki-xml-form-radiocheck (xml-node type)
  (let ((store nil)
        (ecell nil)
        (name  nil)
        (seen    t)
        (attr (xml-node-attributes xml-node)) )
    ;;(message "RCB: %S %S %S" type attr name)
    (setq name  (cdr (assq 'name attr))
          store (if (equal type "radio") 
                    'wiki-xml-form-radio 'wiki-xml-form-check)
          ecell (or (assoc name (symbol-value store))
                    (setq seen nil)
                    (car (add-to-list store (cons name nil)))) )
    (setcdr ecell (cons xml-node (cdr ecell)))
    (or seen
        (setq wiki-xml-form-elems 
              (cons (cons type name) wiki-xml-form-elems))) ))

(defun wiki-xml-form-elems (xml-node)
  "Descend xml.el style structure XML-NODE, storing only form related 
nodes. Internal function used by `wiki-xml-form-data-1'."
  (if (stringp xml-node) nil
    (let ((tag (xml-node-name xml-node)) (type nil))
      (cond
       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ((eq tag 'input)
        (setq type (cdr (assq 'type (xml-node-attributes xml-node))))
        ;; radio and checkbox are "scatterable" items, special handling
        ;; required to accumulate them into contiguous blocks:
        (if (member type '("checkbox" "radio"))
            (wiki-xml-form-radiocheck xml-node type)
          (setq wiki-xml-form-elems (cons xml-node wiki-xml-form-elems))) )
       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ((eq tag 'select) 
        (setq wiki-xml-form-elems 
              (cons (wiki-xml-form-select xml-node) wiki-xml-form-elems)) )
       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ((eq tag 'textarea) 
        (setq wiki-xml-form-elems 
              (cons xml-node wiki-xml-form-elems)) )
       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ;; not a form element, descend to its children:
       (t (mapc 'wiki-xml-form-elems (xml-node-children xml-node))) )) ))

(defun wiki-xml-form-select (xml-node)
  (delq nil (mapcar (lambda (x) (if (stringp x) nil x)) xml-node)))

(defun wiki-xml-form-element-value (element)
  "Given an xml.el style structure representing a form element node, return
a (name . value) cons for text, submit, hidden, select and textarea node types,
or nil for other node types."
  (let (attr tag type name value multi option)
    (setq tag   (xml-node-name       element)
          attr  (xml-node-attributes element)
          type  (cdr (assq 'type     attr))
          name  (cdr (assq 'name     attr))
          multi (cdr (assq 'multiple attr))
          value (cdr (assq 'value    attr)))
    (cond ((eq tag 'input) 
           (cond 
            ((member type 
                     '("text"  "password" "submit"   "hidden" 
                       "image" "reset"    "checkbox" "radio" ))
             (cons name value))
            ;;((equal type "text"    ) (cons name value))
            ;;((equal type "password") (cons name value))
            ;;((equal type "submit"  ) (cons name value))
            ;;((equal type "hidden"  ) (cons name value))
            (t (message "skipping %S (input type %S)" name type) nil) ))
          ((eq tag 'select)
           (setq option 
                 (lambda (o) 
                   (let (oattr ovalue olabel osel) 
                     (setq oattr  (xml-node-attributes o)
                           olabel (xml-node-children   o)
                           osel   (cdr (assq 'selected oattr))
                           ovalue (cdr (assq 'value    oattr)))
                     (or ovalue (setq ovalue olabel))
                     (if multi (when osel (setq value (cons ovalue value)))
                       (when (or (not value) osel) (setq value ovalue))) )) )
           (mapc option (xml-node-children element)) 
           (cons name value))
          ((eq tag 'textarea) (cons name (car (xml-node-children element))))
          (t (message "skipping tag %s" tag) nil) ) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
(defun wiki-autoload (&optional flavour) 
  "Load the wiki-FLAVOUR extension. FLAVOUR defaults to `wiki-flavour'."
  (let ( (target (intern (format "wiki-%s" (or flavour wiki-flavour)))) ) 
    (or (and target (require target)) 
        (error "target %s not loaded for %s" target wiki-flavour)) ))

(defun wiki-var (slot)
  "Look up the flavour-specific variable for a given slot:
returns the variable\'s symbol, the \"base\" wiki-+ symbol, or nil:\n
\(wiki-var 'xlink-regex). Uses `wiki-flavour' to determine the flavour."
  (let ( (target  (intern-soft (format "wiki-%s-%s" wiki-flavour slot))) 
         (default (intern-soft (format "wiki-+-%s" slot))) )
    (or (and target  (boundp target ) target )
        (and default (boundp default) default)) ))

(defun wiki-fun (slot)
  "Look up the flavour-specific function for a given slot:
returns the function\'s symbol, the default symbol, or nil:\n
\(wiki-fun 'target-at-point). Uses `wiki-flavour' to determine the flavour."
  (let ( (target  (intern-soft (format "wiki-%s-%s" wiki-flavour slot)))
         (default (intern-soft (format "wiki-+-%s" slot))) )
    ;;(message "resolving function: %S => %S %S" slot target default)
    (or (and target  (fboundp target ) target )
        (and default (fboundp default) default)
        (error "function slot %S missing" slot)) ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki http request and callback functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-fetch (url callback &optional cbargs)
  "Fetch a wiki page for editing. URL may be a string or a preparsed
url vector. CALLBACK is the function to call in the response buffer
once the response has been received and inserted into it. CBARGS is
a list of arguments to be passed to the callback function."
  (message "called wiki-fetch: wiki-name == %S" wiki-name)
  (let ( (wiki-fetching-url   t)
         (data-buffer       nil) )
    (url-do-setup)
    (url-register-auth-scheme "basic" 'url-basic-auth 5)
    (message "calling url-retrieve: wiki-name == %S; %S" wiki-name url)
    (setq data-buffer (url-retrieve url callback cbargs))
    (save-excursion
      (set-buffer data-buffer)
      (set (make-local-variable 'url-registered-auth-schemes)
	   url-registered-auth-schemes)
      (set (make-local-variable 'wiki-fetching-url) t)) ))
;;    (set (make-local-variable 'url-passwd-entry-func) 'wiki-default-pass)

(defun wiki-http-encoding (header)
  "Extract the encoding from a url.el HTTP buffer heading string HEADER."
  (or (when (string-match "charset=\\([^; \t\r\n]+\\)" header) 
        (intern-soft (downcase (match-string 1 header)) ))
      'utf-8))

(defun wiki-edit-callback (&rest stuff)
  "Once the edit page has been fetched, this does the work of parsing
the form and setting up the wiki edit buffer."
  (let ((case-fold-search  t)
        (ctype  "text/plain")
        (dummy           nil)
        (enc          'utf-8)
        (xhtml           nil)
        (forms           nil)
        (wform           nil)
        (values          nil)
        (texts           nil)
        (title           nil)
        (flavour         nil)
        (wname           nil)
	(inhibit-read-only t))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; clean up any stray properties in the buffer before we start:
    (beginning-of-buffer)
    ;;(message "zap-invisible") (sit-for 0)
    (wiki-zap-invisible) ;; remove http-related inline meta-data
    (beginning-of-buffer)
    (if (setq wname (or wiki-name (cadr (memq :name stuff))))
        (if (setq flavour 
                  (cdr (assq :type (cdr (assoc wname wiki-known-wikis))) ))
              (setq wiki-name    wname
                    wiki-flavour flavour)
          (error "Unknown wiki name %S: no flavour" wname))
      (error "Called without a specific wiki in mind: wiki-edit-callback"))
    ;;(message "wiki-type OK") (sit-for 0)
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; parse the html form data out of the returned buffer
    (setq ctype  url-http-content-type
          enc    (wiki-http-encoding ctype))
    ;;(message "encoding OK") (sit-for 0)
    (set-buffer-multibyte t)
    (decode-coding-region (point-min) (point-max) enc)
    ;;(message "decoding OK") (sit-for 0)
    (message "fixing up known xhtml errors in %S wikis" wiki-flavour)
    (funcall (wiki-fun 'fix-bogus-xhtml))
    (message "done with fixup")
    (write-region (point-min) (point-max) "/tmp/wiki-edit.debug.xml")
    (with-timeout 
        (30 (error "Timeout parsing xml"))
            (setq xhtml  (xml-parse-region (point-min) (point-max))
                  forms  (wiki-xml-form-data xhtml)
                  wform  (funcall (wiki-fun 'choose-form) forms)
                  values (funcall (wiki-fun 'form-values) wform)
                  texts  (funcall (wiki-fun 'form-texts ) wform)
                  title  (funcall (wiki-fun 'page-title ) wform xhtml)
                  ebuf   (funcall (wiki-fun 'buffer-name) wform values title) ))
    (message "parsed xhtml buffer")
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; create (if need be) the edit buffer, zap any old crap in there,
    (setq ebuf (get-buffer-create ebuf))
    (pop-to-buffer ebuf)
    (erase-buffer)
    ;; set the buffer local wiki page meta data up:
    (setq wiki-name      wname
          wiki-flavour   flavour
          wiki-edit-data (list (cons :url (cadr (memq :url stuff)))
                               (cons :form   wform )
                               (cons :title  title )
                               (cons :values values)
                               (cons :texts  texts )) )
    ;; set up buffer hooks
    (add-hook 'kill-buffer-hook 'wiki-kill-related nil :local)
    ;; add the wiki edit data widgets
    (message "adding widgets")
    (mapcar (lambda (text) (wiki-text-section text)) texts)
    ;; we're done. hand over to the user:
    (funcall (wiki-fun 'mode)) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wiki interactive functions for the ui:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-diff ()
  "Diff the buffer with its original content."
  (interactive)
  (if (not wiki-edit-data) (error "wiki: Not a valid wiki-edit buffer"))
  (let ( (page        (cdr (assoc :title wiki-edit-data)))
         (content-var (symbol-value (wiki-var 'content-variable)))
         (old-data     nil)
         (new-data     nil)
	 (caught       nil)
	 (done         nil)
	 (old-filename nil)
	 (new-filename nil)
	 (diff-buffer  nil))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; extract the old and new contents from the old data and new buffer:
    (old-data (cdr (assoc content-var (assoc :texts wiki-edit-data))))
    (new-data (cdr (assoc content-var (wiki-parse-buffer)          )))

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (while (not done)
      (setq old-filename (make-temp-name (format "/tmp/old:%s" page)))
      (condition-case caught
	  (progn
	    (write-region old-data nil old-filename nil 0 nil :o-excl)
	    (setq done t))
	(error nil)) )
    (setq done nil)
    (while (not done)
      (setq new-filename (make-temp-name (format "/tmp/new:%s" page)))
      (condition-case caught
	  (progn
	    (write-region new-data nil new-filename nil 0 nil :o-excl)
	    (setq done t))
	(error nil)) )
    (require 'diff)
    (diff old-filename new-filename)
    ;(delete-file old-filename)
    ;(delete-file new-filename)
    )
  )

(defun wiki-idiff ()
  "Interactively diff your changes to a wiki buffer with the original data."
  (interactive)
  (if (not wiki-edit-data) 
      (error "wiki: Not a valid wiki-edit buffer"))
  (let ( (old-buffer nil)
	 (new-buffer nil)
	 (old-texts  (cdr (assoc :texts wiki-edit-data))) )
    ;; create the "old version" buffer and add it to the list of secondary
    ;; buffers related to this one:
    (setq new-buffer (current-buffer)
          old-buffer (get-buffer-create
                      (replace-regexp-in-string "\\[.+\\]"
                                                "[ORIGINAL]" (buffer-name))))
    (setq wiki-twin-buffers (cons old-buffer wiki-twin-buffers))

    ;; clear out any old garbage in the old-version buffer:
    (set-buffer old-buffer)
    (let ((inhibit-read-only t)) 
      (widen)
      (delete-region (point-min) (point-max)))

    ;; insert widgets for the old data:
    (mapcar (lambda (text) (wiki-text-section text)) old-texts)

    ;; now shamelessly piggy back on someone else's hard work.
    ;; I mean, re-use code, yes, that's what I mean...
    (require 'ediff)
    (ediff-buffers old-buffer new-buffer) ))

(defun wiki-follow ()
  "Edit the wiki page pointed at by the wiki link at the current position."
  (interactive)
  (let ( (target (or (funcall (wiki-fun 'target-at-point) (point))
		     (wiki-edit-read-target) )) )
    (wiki-edit target) ))

(defun wiki-edit-read-target (&optional wiki)
  "Read a target wiki page name to edit from the user. WIKI defaults to 
`wiki-name' if not explicitly supplied."
  (let* ((wiki-name wiki)
         (wiki-flavour (cdr (assq :type (cdr (assoc wiki wiki-known-wikis)))))
         (page nil)
         (hvar nil)
         (minibuffer-local-completion-map minibuffer-local-completion-map))
    (define-key minibuffer-local-completion-map " " 'self-insert-command)
    (setq hvar (wiki-var 'target-history)
          ;;dummy (message "hvar: %S %S; %S" 
          ;;               wiki-name hvar (symbol-value hvar))
          page  (completing-read 
                 "page: " (symbol-value hvar) nil nil nil hvar "" t)) ))
;;        page (read-string "page: " nil hvar "" t)) ))

(defun wiki-edit-read-wiki (&optional type)
  "Read a wiki configuration name (See `wiki-known-wikis') from the user, 
offering the user the opportunity to configure a new wiki if they enter a
wiki that is not in `wiki-known-wikis'."
  (let ((history-add-new-input nil) 
        (name-history          nil)
        (wiki                  nil))
    (setq name-history 
          (if type 
              (delete nil
                      (mapcar 
                       (lambda (w) 
                         (and (eq type (cdr (assq :type (cdr w)))) (car w)))
                       wiki-known-wikis))
            (mapcar 'car wiki-known-wikis)))
    (setq wiki
          (completing-read 
           "wiki: " 
           name-history 
           nil 
           nil 
           (or wiki-name wiki-current-name (car name-history)) 
           'name-history ))
          ;;(read-string "wiki: " nil 'name-history "" nil))
    (if (assoc wiki wiki-known-wikis) 
        wiki ;; known wiki, name is ok to return
      ;; blergh, the user wrote something we haven't seen before
      ;; see if they want to set it up now:
      (when (y-or-n-p (format "unknown wiki '%s' - configure? " wiki))
        (let ( (history-add-new-input t)  
               (type-history nil)
               (base-history nil)
               (user-history nil)
               (new-type    type) 
               new-base new-user url )
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          ;; get the type and make sure it's one we know about:
          (while (not (locate-library (format "wiki-%s" new-type)))
            (and new-type 
                 (with-temp-message (format "Unknown type: `%s'" new-type)
                   (sit-for 0)))
            (setq new-type 
                  (intern (read-string "type: " nil 'type-history "" nil))) )
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          ;; get the base url and make sure it parses (and has enough info)
          (while (progn (setq url (url-generic-parse-url new-base))
                        (not (and (member  (url-type url) '("http" "https"))
                                  (stringp (url-host url))
                                  (< 0 (length (url-host url))) )))
            (and new-base 
                 (with-temp-message (format "Bad URL `%s'" new-base) 
                   (sit-for 1)))
            (setq new-base
                  (read-string "base url: " nil 'base-history "http://" nil)) )
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          ;; read the user name, must be a string, is allowed to be empty:
          (while (not (stringp new-user))
            (setq new-user
                  (read-string "username: " nil 'user-history 
                               (or (url-user url) (user-login-name)) nil)) )
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          ;; finally. save it:
          (setq new-wiki `(,wiki (:type     . ,new-type)
                                 (:base-url . ,new-base) 
                                 (:username . ,new-user)))
          (setq new-wiki (cons new-wiki wiki-known-wikis)) 
          (customize-set-variable 'wiki-known-wikis new-wiki))
          wiki)) ))

(defun wiki-edit (target &optional wiki)
  "Edit a wiki page. The target need not be in any particular capitalisation:
words separated by spaces or underscores should work equally well.\n
Asynchronously fetches the wiki page in question, then displays a buffer
allowing you to edit the text and graveyard.\n
You may have more than one page open at once.\n
WIKI defaults to `wiki-name' if not supplied.
See also: `wiki-save'"
  (interactive 
   (if wiki-name
       (list (wiki-edit-read-target) nil)       
     (nreverse (list (wiki-edit-read-wiki) (wiki-edit-read-target))) ))
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; make sure we know what we're pointed at:
  (or wiki 
      (setq wiki wiki-name)
      (error "No wiki selected, either explicitly or via `wiki-name'"))

  (let* ( (wiki-flavour (cdr (assq :type (cdr (assoc wiki wiki-known-wikis))) ))
          (wiki-name  wiki)
          (edit-url   nil)
          (cbargs     nil)
          (url-string nil)
          (url-vector nil) )
    (wiki-autoload)
    (setq edit-url (wiki-fun 'edit-url))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; construct the target url and then parse it:
    (setq url-string (funcall edit-url wiki 
                              (funcall (wiki-fun 'uglify-string) target))
          url-vector (url-generic-parse-url url-string))
    (message "wiki-edit: URL-STRING: %s" url-string)
    (message "wiki-edit: URL-VECTOR: %s" url-vector)
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; zap the username if it was specified in the url (gets ignored anyway):
    ;; zap the password if it's embedded in the url
    (and (url-user     url-vector) (url-set-user url-vector nil))
    (and (url-password url-vector) (url-set-password url-vector nil))
    (setq cbargs (list :name wiki :url url-vector))
    (setq wiki-current-name wiki-name)
    (wiki-fetch url-vector 'wiki-edit-callback cbargs) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define wiki-mode based on text-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defface wiki-struck-through '((default (:strike-through t))) 
  "Face for showing the strike-through markup of the wiki wiki")

(defface wiki-head-5 
  '((default (:height 1.2 :inherit variable-pitch))) "wiki heading 5")
(defface wiki-head-4 
  '((default (:height 1.2 :inherit wiki-head-5   ))) "wiki heading level 4")
(defface wiki-head-3 
  '((default (:height 1.2 :inherit wiki-head-4   ))) "wiki heading level 3")
(defface wiki-head-2 
  '((default (:height 1.2 :inherit wiki-head-3   ))) "wiki heading level 2")
(defface wiki-head-1 
  '((default (:height 1.2 :inherit wiki-head-2   ))) "wiki heading level 1")

(defface wiki-superscript 
  '((default (:overline  "red" :height 0.5))) "wiki superscript")
(defface wiki-subscript   
  '((default (:underline "red" :height 0.5))) "wiki subscript")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (makunbound 'wiki-mode-keywords)

(defun wiki-define-mode (flavour)
  "Define a wiki mode for FLAVOUR. Designed to be used by wiki-FLAVOUR.el
files (See wiki-+.el for the basic set of functions a FLAVOUR file may choose
to define)."
  (let ( (wiki-flavour flavour)
         (mode-symbol  (intern (format "wiki-%s-mode"      flavour)))
         (mode-map     (intern (format "wiki-%s-mode-map"  flavour)))
         (mode-hook    (intern (format "wiki-%s-mode-hook" flavour)))
         (mode-label   (format "Wiki[%s]" flavour)) 
         (mode-syntactic-keywords nil)
         (syntactic-keywords      nil)
         (flock-keywords          nil)
         (mode-keywords           nil)
         (mode-font-lock          nil))
    (setq mode-keywords           (wiki-var 'mode-keywords)
          mode-syntactic-keywords (wiki-var 'mode-syntactic-keywords)
          syntactic-keywords      (copy-sequence 
                                   (symbol-value mode-syntactic-keywords))
          flock-keywords     
          (list (list mode-keywords)
                nil
                nil
                '((?_ . " "))
                nil
                (cons 'font-lock-syntactic-keywords syntactic-keywords) ))
    (set mode-map (funcall (wiki-fun 'mode-map)))
    (eval
     `(define-derived-mode ,mode-symbol text-mode ,mode-label
        (setq font-lock-defaults  ',flock-keywords
              font-lock-multiline  t)
        (font-lock-set-defaults)
        (turn-on-font-lock)) )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;e;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FIXUP code for bugs in the url libraries:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'wiki)
