;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; trac wiki support
(defconst wiki-trac-xlink-regex
  (concat
   "\\<\\([a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\)\\>"
   "\\|"
   "\\[wiki:\\(.+?\\)\\(?:\\s-+.+\\)?\\]"))

(defun wiki-trac-target-at-point (point)
  "Return the string surrounding POINT if it is a wiki link, or nil."
  (save-excursion
    (let ((end nil) (x t) (target nil) (case-fold-search nil))
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      ;; search forwards till we find an xref that spans point:
      (while (and x (search-forward-regexp wiki-trac-xlink-regex end :no-error))
        (and (<= (or (match-beginning 1) (-  (match-beginning 2) 5)) point)
             (>= (or (match-end       1) (1+ (match-end       0)  )) point)
             (setq x nil target (or (match-string 1) (match-string 2)))))
      ;; make sure we don't match [[macros]]
      (if (and target (eq (aref target 0) ?\[)) nil (concat "'" target))) ))

(defun wiki-trac-choose-form (forms)
  (let ((form nil)) 
    (mapc (lambda (F) 
            (if (equal "edit" (cdr (assq 'id (cadr F)))) (setq form F))) forms)
    form))

(defconst wiki-trac-ignore-elements   '("cancel" "preview" "diff"))
(defconst wiki-trac-special-variables '(("scroll_bar_pos" . 0)))
(defconst wiki-trac-content-variable   "text")

(defun wiki-trac-form-element-value (element)
  (let ((attr (xml-node-attributes element)))
    (when (and (not (eq 'textarea (xml-node-name element)))
               (not (member (cdr (assq 'name attr)) wiki-trac-ignore-elements)))
      (or (assoc (cdr (assq 'name attr)) wiki-trac-special-variables) 
          (wiki-xml-form-element-value element)) ) ))

(defun wiki-trac-form-values (form)
  (delete nil (mapcar 'wiki-trac-form-element-value (xml-node-children form)) ))

(defun wiki-trac-page-title (form xhtml)
  (let ( (action (cdr (assq 'action (xml-node-attributes form)))) 
         (title  nil) )
    (or action (error "No action found in form %S" (xml-node-attributes form)))
    (if (string-match ".+/\\([^/]+\\)$" action) 
        (setq title (match-string 1 action) title (url-unhex-string title))
      (error "No title found in target %S" action)) ))

(defun wiki-trac-buffer-name (form values title)
  (format "*wiki-trac: %s [%s]*"
          (funcall (wiki-fun 'fix-string) title) 
          (cdr (assoc "__FORM_TOKEN" values))) )

(defconst wiki-trac-response-msg-regex
  "<div class=\"system-message\">[ \n]+\\(\\(?:.\\|\n\\)*?\\)[ \n]+</div")

(defun wiki-trac-display-response (&optional status-list page buffer) 
  "If possible, fillet the wiki response and display it in the message area.
Otherwise show the whole response."
  (save-excursion
    (let ((start  (point-min))
          (re-editing nil)
          (title-re "<h1>Editing \"?\\(.*?\\)\"?</h1>"))
      ;; look for message we understand:
      (goto-char start)
      (when (search-forward-regexp wiki-trac-response-msg-regex nil :no-error)
        (message "%s" (match-string 1)) )
      ;; are we re-editing? or are we ok?
      (cond
       ;; whoops, we collided:
       ((progn (goto-char start) (search-forward-regexp title-re nil :no-error))
        (setq re-editing t)
        (message "Re editing: %s" 
                 (wiki-compact-string (wiki-strip-tags (match-string 1))) )
        (wiki-edit (concat "'" (match-string 1))) )
       ;; we're ok (this is now a no-op).
       (t nil ) )

      ;; if we collided, rename the old buffer instead of killing it:
      (if (bufferp buffer)
          (if re-editing
              (let ((cbuf (current-buffer)))
                (set-buffer buffer)
                (rename-buffer
                 (replace-regexp-in-string "\\[.+\\]\\*$"
                                           "[OLD]" (buffer-name)))
                (set-buffer cbuf))
            (message "Saved wiki-trac buffer: %S" buffer)
            (with-current-buffer buffer (bury-buffer)) )) ))
  ;; this kills the http data buffer, may not be necessary now that
  ;; url-http-parse-headers is fixed below:
  (kill-buffer (current-buffer) ))

(defun wiki-trac-edit-url (wiki page) 
  (let ( (target (url-hexify-string page))
         (base   (cdr (assq :base-url (assoc wiki wiki-known-wikis)))) )
    (setq target (replace-regexp-in-string "%2f" "/" target))
    (concat base target "?action=edit")))

(defvar wiki-trac-mode-keywords
  '( ("^<[^<>]+>" (0 'mode-line t))
     ("\\(?:^\\|[^!]\\)\\<[A-Z][a-zA-Z]+\\(?:[A-Z][a-z]+\\)+\\>" 
      . font-lock-variable-name-face)
;;    . 'hi-red-b)
     ("\\(?:^\\|[^!]\\)\\(\\[\\)\\([^[ \t].+?\\)\\(\\]\\)"
      (1 font-lock-builtin-face t)
      (2 font-lock-variable-name-face t)
;;      (2 'hi-red-b t)
      (3 font-lock-builtin-face t))
     ("\\(?:^\\|[^!=]\\)\\(=\\{1,5\\}\\)\\(.*?[^!]\\)\\(\\1\\)\\(?:[^=]\\|$\\)"
      (1 font-lock-builtin-face t)
      (2 (intern-soft (format "wiki-head-%d" (length (match-string 1)))) append)
      (3 font-lock-builtin-face t))
     ("\\(?:^\\|[^!']\\)\\('''''\\|'''\\|''\\)\\(.*?[^!]\\)\\(\\1\\)"
      (1 font-lock-builtin-face t)
      (2 (let ( (markup (length (match-string 1))) )
           (cond ((eq markup 5) 'bold-italic)
                 ((eq markup 3) 'bold       )
                 ((eq markup 2) 'italic     ))) prepend)
      (3 font-lock-builtin-face t))
     ("\\(?:^\\|[^!_]\\)\\(__\\)\\(.*?[^!]\\)\\(__\\)"
      (1 font-lock-builtin-face t)
      (2 'underline prepend)
      (3 font-lock-builtin-face t))
     ("\\(?:^\\|[^!]\\)\\(~~\\)\\(.*?[^!]\\)\\(~~\\)"
      (1 font-lock-builtin-face t)
      (2 'wiki-struck-through prepend)
      (3 font-lock-builtin-face t))
     ("\\(?:^\\|[^!]\\)\\(\\^\\)\\(.*?[^!]\\)\\(\\^\\)"
      (1 font-lock-builtin-face t)
      (2 'wiki-superscript prepend)
      (3 font-lock-builtin-face t))     
     ("\\(?:^\\|[^!]\\)\\(,,\\)\\(.*?[^!]\\)\\(,,\\)"
      (1 font-lock-builtin-face t)
      (2 'wiki-subscript prepend)
      (3 font-lock-builtin-face t))     
     ("||"             (0 font-lock-string-face   t))
     ("\\(||\\)\\s-*$" (1 font-lock-builtin-face  t))
     ("^\\s-*\\(||\\)" (1 font-lock-builtin-face  t))
     ("\\(\\[\\[\\)\\(.+?\\)\\(\\]\\]\\)"
      (1 font-lock-builtin-face       t)
      (2 font-lock-function-name-face t)
      (3 font-lock-builtin-face       t))
     ("\\(?:[^-]\\|^\\)----\\(?:[^-]\\|$\\)" . font-lock-builtin-face)
     )
  "The `wiki-mode' setting for `font-lock-defaults.'")

(wiki-define-mode 'trac)

(provide 'wiki-trac)
