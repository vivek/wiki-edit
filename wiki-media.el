;; wiki-edit (wiki.el) allows editing & syntax highlighting of wiki pages
;; Copyright © 2009-2019 Vivek Das Mohapatra <vivek@etla.org>

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;; more details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(require 'wiki-form)

(defun wiki-media-uglify-string (string)
  "Take a \"foo bar\" string and return its wiki word form.
This function is called \"uglify\" because it is normally used on the 
StUpidLyCapitaLised words favoured by other wikis."
  (if (string-match "^'\\(.+\\)" string)
      (match-string 1 string)
    (replace-regexp-in-string " " "_" string)) )

(defun wiki-media-fix-string (string)
  "Take a wiki word and expand into the \"friendly\" form."
  (let ((case-fold-search nil))
    (replace-regexp-in-string "_" " " string :fixed-case) ))

(makunbound 'wiki-media-xlink-regex)
(defconst wiki-media-xlink-regex
  (concat "\\(#REDIRECT[ \t]+\\)?"
          "\\[\\[\\(?::\\(Category\\|Help\\):\\)?"
          "\\(#?\\)\\([^:|\n]+?\\)?\\(?:|\\(.*?\\)\\)?\\]\\]" ))

(defun wiki-media-target-at-point (point)
  "Return the string surrounding POINT if it is a wiki link, or nil."
  (save-excursion
    (let ((end nil) (x t) (target nil) (case-fold-search nil))
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      ;; search forwards till we find an xref that spans point:
      (while (and x (search-forward-regexp wiki-media-xlink-regex end t))
        (and (<= (match-beginning 0) point)
             (>= (match-end       0) point)
             (not (equal "#" (match-string 3)))
             (setq x      nil 
                   target (concat (match-string 2) (match-string 4))) ))
      (if target 
          (replace-regexp-in-string " " "_" (substring-no-properties target)) 
        nil) )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; table slice-and-dice support:
(defun wiki-media-table-seek-table-re ()
  "Regex to look for the start of the table."
  "^{|")

(defun wiki-media-table-seek-start ()
  "Find the start of the current table"
  (let ((goal (point)))
    (save-excursion
      (beginning-of-line)
      (if (looking-at "{|") 
          (setq goal (point))
        (setq goal (search-backward-regexp "^{|" nil t)) ))
    (goto-char goal) ))

(defun wiki-media-slurp-row (&optional cellfun)
  (let ((row-data nil) (row-text nil) (row-start (point)) (row-end nil))
    (or cellfun (setq cellfun 'identity))
    (search-forward-regexp "|-\\||}")
    (setq row-end (1- (match-beginning 0)))
    (goto-char row-start)
    (forward-line)
    (setq row-text (buffer-substring-no-properties (point) row-end)
          row-text (replace-regexp-in-string 
                    "^|\\([^|\n]*|\\)[^|]" "" row-text nil nil 1)
          row-text (replace-regexp-in-string 
                    "^!\\([^!\n]*!\\)[^!]" "" row-text nil nil 1))
    (mapcar 
     (lambda (cell) (setq row-data (cons (funcall cellfun cell) row-data))) 
     (split-string row-text "\\(^\\|\n\\)[|!]\\|[|!]\\{2\\}" t))
    (goto-char row-start)
    (nreverse  row-data ) ))

(defun wiki-media-strip-markup (text)
  (replace-regexp-in-string "\\s-+" " "
   (replace-regexp-in-string 
    "</?.*?>\\|\\[\\[\\|\\]\\]\\|\\[.*?\\]\\|^[;:]\\|=+\\|'+\\|\n" "" text) ))

(defun wiki-media-table-forward-row (&optional end)
  (interactive)
  (and (or (re-search-forward "\\(^{|.*\\)\n\\(|[^+-]\\|!\\)"   end t)
           (re-search-forward "\\(^|\\+.*\\)\n\\(|[^+-]\\|!\\)" end t)
           (re-search-forward "\\(^|-\\)"                       end t))
       (goto-char (match-end 1)) ))

(defun wiki-media-table-data (&optional cellfun rowfun)
  (let ((table nil) 
        (start nil) 
        (end   nil))
    (or rowfun (setq rowfun 'identity))
    (save-excursion
      (funcall (wiki-fun 'table-seek-start))
      (setq start (point))
      (when (setq end (search-forward-regexp "^|}" nil t))
        (goto-char start)
        (while (wiki-media-table-forward-row end)
          (setq table 
                (cons (funcall rowfun (wiki-media-slurp-row cellfun)) table)))
        ))
    (nreverse table)))

(defun wiki-media-table-seek-row-start ()
  "Find the start of this row, or, if before the first row, the start of that 
row, or, if we are at the end of the table (after the last row) find the 
correct point at which a new row would need to be inserted."
  (interactive)
  (let ((here nil) (table-end nil) (start nil) (continue t) (table-start nil)) 
    (beginning-of-line)
    (setq here (point))
    (when (not (looking-at "|-\\||}"))
      ;; not at the end of the table, or already at the boundary
      ;; between two rows => need to find a suitable starting point
      (funcall (wiki-fun 'table-seek-start))
      (setq table-start (point))
      ;; if we can find the end of the table, zip forwards from the start 
      ;; to the end to find the row start _just_ before our starting point
      (when (setq table-end (search-forward-regexp "^|}" nil t))
        (goto-char table-start)
        (while (and continue (<= (point) here))
          (setq start (point))
          (setq continue (wiki-media-table-forward-row table-end)))
        ;; oops, we wound up at the start of the table before the 1st row:
        (when (= table-start start) 
          (goto-char start)
          (setq start (wiki-media-table-forward-row)))
        (when (not start) (error "Table parse error: cannot find row start"))
        ;; huzzah, we found the start of this row (or the first row):
        (goto-char start))) 
    (beginning-of-line) 
    (point)))

(defun wiki-media-table-add-row ()
  (interactive)
  (when (wiki-media-table-seek-row-start)
    (let ((headers (wiki-media-table-headers)))
      ;; add the new row based on the headers:
      (insert "|-\n")
      (mapc (lambda (c) (insert "| " c "\n")) headers) )) )

(defun wiki-media-table-add-column ()
  (interactive)
  (message "sorry, add-column not implemented yet"))

(defun wiki-media-table-del-column ()
  (interactive)
  (message "sorry, del-column not implemented yet"))

(defun wiki-media-table-wherami ()
  (interactive)
  (message "sorry, table-whereami not implememented yet"))

(defun wiki-media-table-headers ()
  (car (wiki-media-table-data 'wiki-media-strip-markup)))

(defun wiki-media-table-align ()
  (interactive)
  (message 
   "Sorry, mediawiki doesn't use a tabular markup for tables, cannot align"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun wiki-media-fix-bogus-xhtml () )
(defun wiki-media-fix-bogus-xhtml-old-flaky-attempt-to-support-broken-xhtml ()
  (save-excursion
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; missing quotes around atributes
    (beginning-of-buffer)
    (while (re-search-forward "<\\S-+ \\([^>]+\\)>" nil t)
      (let ((end (1+ (match-end 0))))
        (goto-char (match-beginning 1))
        ;;(message "(match-string 1): %S" 
        ;;         (substring-no-properties (match-string 1)))
        (while (re-search-forward 
                (concat "\\(\\S-+?=\\)\\s-*"
                        "\\(?:"
                        "\\([\"']\\)\\(\\(?:.\\|\n\\)+?\\)\\2"
                        "\\|\\(\\S-*\\)"
                        "\\)") end t)
          (if (match-string 4) 
              (replace-match "\\1\"\\4\"")) ) )) 
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; unterminated tags: (not a problem in standard mediawiki templates,
    ;; but a tleast 1 alpha tester has to deal with this bogosity):
    (beginning-of-buffer)
    (while (re-search-forward "<\\([a-z]+\\)\\([^>]*\\)\\(/\\)?>" nil t)
      (when (and (member (match-string 1) '("meta" "link" "img" "input" "br"))
                 (not (match-string 3)))
        (replace-match "<\\1\\2/>")))
    ))

(defun wiki-media-choose-form (forms)
  (let ((form nil)) 
    (mapc 
     (lambda (F) 
       (if (equal "editform" (cdr (assq 'id (cadr F)))) (setq form F))) forms)
    form))

(defconst wiki-media-ignore '("wpDiff" "wpPreview"))

(defun wiki-media-form-element-value (element)
  (let ((attr (xml-node-attributes element)))
    (when (and (not (eq 'textarea (xml-node-name element)))
               (not (member (cdr (assq 'name attr)) wiki-media-ignore)))
      (wiki-xml-form-element-value element) ) ))

(defun wiki-media-form-values (form)
  (delete nil 
          (mapcar 'wiki-media-form-element-value (xml-node-children form)) ))

(defun wiki-media-page-title (form xhtml)
  (let ((title nil) 
        (args  nil)
        (attr (xml-node-attributes form)))
    (setq args (cadr (split-string (cdr (assq 'action attr)) "\\?")))
    (mapc 
     (lambda (x) 
       (let ((var (split-string x "=")))
         (when (equal "title" (car var)) (setq title (cadr var))) ))
     (split-string args "[&;]")) 
    (replace-regexp-in-string " " "_" title) ))

(defun wiki-media-buffer-name (form values title)
  (format "*wiki-media: %s [%s]*"
          (funcall (wiki-fun 'fix-string) title) 
          (cdr (assoc "wpEdittime" values))) )

(defun wiki-media-display-response (&optional status-list page buffer)
  "Check for a collision and re-edit if there was one, otherwise say we're Ok."
  (let ( (xhtml   nil)
         (forms   nil) 
         (re-edit nil) )
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; copy magic buffer-local values back in from the original edit buffer
    (let (f n)
      (save-excursion (set-buffer buffer) (setq f wiki-flavour n wiki-name))
      (setq wiki-flavour f wiki-name n))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (wiki-fun 'fix-bogus-xhtml)
    (setq xhtml   (xml-parse-region   (point-min) (point-max)) 
          forms   (wiki-xml-form-data xhtml)
          re-edit (funcall (wiki-fun 'choose-form) forms))
    (if re-edit 
        (let ((cbuf (current-buffer)))
          (set-buffer buffer)
          (rename-buffer
           (replace-regexp-in-string "\\[.+\\]" "[OLD]" (buffer-name)))
          (set-buffer cbuf)
          (wiki-edit-callback))
      (message "killing mediawiki buffer: %S" buffer)
      (kill-buffer buffer)) ))

;;http://localhost/mediawiki/index.php?title=Help:Contents&action=edit
(defun wiki-media-edit-url (wiki page)
  (let ( (target (url-hexify-string page))
         (base   (cdr (assq :base-url (assoc wiki wiki-known-wikis)))) )
    (concat base "?action=edit&title=" target)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; login support
(defun wiki-media-login-url (wiki)
  (let ((target (wiki-media-edit-url wiki "Special:Userlogin")))
    (replace-regexp-in-string "\\?action=edit&" "?" target)))

(defun wiki-media-logout-url (wiki)
  (let ((target (wiki-media-edit-url wiki "Special:Userlogout")))
    (replace-regexp-in-string "\\?action=edit&" "?" target)))

(defun wiki-media-login-form (wiki)
  (let ((url-string (wiki-media-login-url wiki)))
    (wiki-fetch url-string 
                'wiki-media-login-form-callback 
                `((:url    . ,url-string)
                  (:submit . wiki-media-submit-callback)) ) ))

(defun wiki-media-choose-login-form (xml-forms)
  (let ((login-form nil))
    (mapc 
     (lambda (form)
       (message "%S" (xml-node-attributes form))
       (when (member "userlogin"
                     (list (cdr (assq 'id   (xml-node-attributes form)))
                           (cdr (assq 'name (xml-node-attributes form)))))
         (setq login-form form))) xml-forms)
    login-form))

(defun wiki-media-login-form-callback (&rest args)
  (let ( (http-buffer (current-buffer))
         (login-page  nil)
         (forms       nil)
         (login-form  nil) )
    (setq login-page (xml-parse-region (point-min) (point-max))
          forms      (wiki-xml-form-data login-page)
          login-form (wiki-media-choose-login-form forms)) 
    (with-current-buffer (wiki-form-xml-data-to-ui login-form)
      (setq wiki-edit-data (cdr args))) 
    (kill-buffer http-buffer) ))

(defun wiki-media-submit-callback (&rest args)
  (let ( (rxhtml (car (xml-parse-region (point-min) (point-max))))
         (errdiv  nil) 
         (errtext nil)
         (errmatch 
          (lambda (n) 
            (equal (assoc 'class (xml-node-attributes n)) 
                   '(class . "errorbox")))) )
    (if (setq errdiv (wiki-xml-matching-children rxhtml errmatch 'div))
        (progn 
          (setq errtext (wiki-xml-node-text (car errdiv))
                errtext (replace-regexp-in-string "\\s-+" " " errtext))
          (with-temp-message (format "[ %s ]" errtext) (sit-for 3))
          nil)
      t) ))

(defun wiki-media-logout ()
  (interactive)
  (wiki-fetch (wiki-media-logout-url wiki-name) 'kill-buffer))

(defun wiki-media-login ()
  (interactive)
  (let ((wiki (or wiki-name (wiki-edit-read-wiki 'media))))
    (wiki-media-login-form wiki)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; syntax highlighting

(defun wiki-media-font-lock-nowiki (limit)
  ;;(message "nowiki: %S -> %S; %s" (point) limit 
  ;;         (buffer-substring-no-properties (point) limit))
  (re-search-forward 
   "\\(<nowiki>\\)\\(\\(?:.\\|\n\\)*?\\)\\(</nowiki>\\)" limit t))

(makunbound 'wiki-media-image-embed-keywords-regexp)
(defconst wiki-media-image-embed-keywords-regexp
  (concat 
   "\\(|\\)"
   "\\(?:" "\\(" "thumb\\(?:nail\\)?"                    "\\|"
                  "frame\\(?:less\\)?"                    "\\|"
                  "left\\|right\\|center\\|none\\|border" "\\)" "\\|"
                  "\\(?:\\([0-9]+\\)\\(px\\)\\)"                "\\|"
                  "\\(.+\\)" "[]|]"                             "\\)"))

(makunbound 'wiki-media-attribute-highlights)
(defconst wiki-media-attribute-highlights
  '( (0 font-lock-builtin-face       t)
     (1 font-lock-variable-name-face t)
     (2 font-lock-string-face        t)) )

(makunbound 'wiki-media-mode-keywords)
(defvar wiki-media-mode-keywords
  `( (,wiki-media-xlink-regex
      (0 font-lock-builtin-face        t)
      (1 font-lock-builtin-face        t :optional)
      (2 font-lock-function-name-face  t :optional)
      (3 font-lock-builtin-face        t :optional)
      (4 font-lock-variable-name-face  t :optional)
      (5 font-lock-string-face         t :optional) )
     ("\\(\\[\\[:?\\)\\(Image\\|Media\\):\\([^]|]+\\).*\\(\\]\\]\\)"
      (1 font-lock-builtin-face        t)
      (2 font-lock-function-name-face  t)
      (3 font-lock-variable-name-face  t)
      (4 font-lock-builtin-face        t)
      (,wiki-media-image-embed-keywords-regexp
       (goto-char (match-end 2)) 
       nil
       (1 font-lock-builtin-face       t )
       (2 font-lock-keyword-face       t :optional)
       (3 font-lock-variable-name-face t :optional)
       (4 font-lock-type-face          t :optional)
       (5 font-lock-string-face        append :optional)))
     ("\\[\\[\\(Category\\|Help\\):.*\\]\\]"
      (0 font-lock-builtin-face        t)
      (1 font-lock-function-name-face  t)
      ("\\([:|]\\)\\([^]|]+\\)" (goto-char (match-end 1)) nil
       (1 font-lock-builtin-face       t)
       (2 font-lock-variable-name-face t)))
     ("http://[a-z0-9A-Z-]+\\(?:\\.[a-zA-Z0-9-]+\\)*\\(?:/\\S-+\\)?"
      (0 font-lock-function-name-face nil))
     ("\\(\\[\\)\\([a-z]+?:///?\\S-+?\\)\\(?:\\s-+\\(.*?\\)\\)?\\(\\]\\)"
      (1 font-lock-builtin-face        t)
      (2 font-lock-function-name-face  t)
      (3 font-lock-variable-name-face  t :optional)
      (4 font-lock-builtin-face        t))
     ("\\(\\[\\)http://{{.*?}}\\(?:/\\S-+\\)?\\(\\]\\)"
      (1 font-lock-builtin-face        t)
      (2 font-lock-builtin-face        t))
     ("\\(?:^\\|[^']\\)\\('''''\\|'''\\|''\\)\\(.*?\\)\\(\\1\\)"
      (1 font-lock-builtin-face        t)
      (2 (let ( (markup (length (match-string 1))) )
           (cond ((eq markup 5) 'bold-italic)
                 ((eq markup 3) 'bold       )
                 ((eq markup 2) 'italic     ))) prepend)
      (3 font-lock-builtin-face t))
     ("^\\(=\\{1,5\\}\\)\\(.*?\\)\\(\\1\\)\\s-*"
      (1 font-lock-builtin-face t)
      (2 (intern-soft (format "wiki-head-%d" (length (match-string 1)))) append)
      (3 font-lock-builtin-face t))
     ("^\\(;\\)\\(.*\\)" 
      (1 font-lock-builtin-face t) 
      (2 font-lock-variable-name-face prepend))
     ("^\\(:\\)\\(.*\\)" 
      (1 font-lock-builtin-face t) 
      (2 font-lock-variable-name-face prepend))
     ("^\\([#*]+\\) " (1 font-lock-keyword-face t))
     ("\\({{\\)\\(.*\\)\\(}}\\)" 
      (1 font-lock-builtin-face t)
      (2 font-lock-preprocessor-face t)
      (3 font-lock-builtin-face t))
     ("^\\([ \t\r]\\)\\(.*\\)$" 
      (1 'highlight   prepend) 
      (2 'fixed-pitch prepend))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; table highlighting. this is less than perfect, but should cover 
     ;; most use cases
     ;; {|   start table
     ;; |+   table caption, optional; one per table and between table start 
     ;;      and first row
     ;; |-   table row, optional on first row -- engine assumes the first row
     ;; !    table header cell, optional. Consecutive table headers
     ;;      may be added on same line separated by double marks (!!)
     ;;      or start on new lines, each with its own single mark (!).
     ;; |    table data cell, required! Consecutive table data cells may
     ;;      be added on same line separated by double marks (||) or
     ;;      start on new lines, each with its own single mark (|).
     ;; |}   end table
     ("^\\(?:{|\\||-\\||\\+\\)"
      ("\\(\\S-+\\)=\"\\([^\"]*\\)\""
       nil
       nil
       ,@wiki-media-attribute-highlights))
     ("^\\([|!]\\)[^}+-]"
      (1 font-lock-builtin-face t)
      ("\\(\\S-+\\)=\"\\([^\"]*\\)\""
       (let ((ret nil) (start (match-end 1))
             (fence (match-string 1))) 
         (setq ret (search-forward fence (line-end-position) t))
         (if (and ret (looking-at fence)) 
             (setq ret (1+ start))
           (setq ret (line-end-position)))
         (goto-char start) ret)
       (beginning-of-line)
       ,@wiki-media-attribute-highlights)
      ("\\(||\\|!!\\)" 
       nil
       (beginning-of-line)
       (1 font-lock-builtin-face)))
     ("^{|"
      (0 font-lock-builtin-face t)
      ("^|\\+" 
       (let ((ret nil) (start (point))) 
         (forward-line 1)
         (setq ret (line-end-position))
         (goto-char start) ret)
       (goto-char (match-end 0))
       (0 font-lock-builtin-face t))
      ("^|-"
       (let ((ret nil) (start (point))) 
         (setq ret (save-match-data (search-forward-regexp "^|}" nil t)))
         (goto-char start) ret)
       (goto-char (match-end 0))
       (0 font-lock-builtin-face t)))
     ("^|}" (0 font-lock-builtin-face t))
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ("<gallery>"
      (0 font-lock-builtin-face t)
      (,(concat 
         "\\(?:" "\\(Image\\)\\(:\\)\\([^|\n]*\\)\\(?:\\(|\\).*\\)?" "\\|"
                 "^\\(</gallery>\\)"                                 "\\)")
       (let ((ret nil) (start (point)))
         (setq ret (search-forward "</gallery>" nil t))
         (goto-char start) ret)
       nil
       (1 font-lock-function-name-face t :optional)
       (2 font-lock-builtin-face       t :optional)
       (3 font-lock-variable-name-face t :optional)
       (4 font-lock-builtin-face       t :optional)
       (5 font-lock-builtin-face       t :optional)))
     ("<pre>"
      (0 font-lock-builtin-face t)
      ("\\(\\(?:.\\|\n\\)*\\)\\(</pre>\\)" 
       (let ((ret nil) (start (point))) 
         (setq ret (search-forward "</pre>" nil t)) 
         (goto-char start) ret)
       nil
       (1 'fixed-pitch t)
       (2 font-lock-builtin-face t)))
     ("<nowiki>"
      (0 font-lock-builtin-face t)
      ("\\(\\(?:.\\|\n\\)*\\)\\(</nowiki>\\)" 
       (let ((ret nil) (start (point))) 
         (setq ret (search-forward "</nowiki>" nil t)) 
         (goto-char start) ret)
       nil
       (1 'default t)
       (2 font-lock-builtin-face t))) ))

(defun wiki-media-mode-map ()
  "Return a keymap for the mediawiki wiki mode."
  (let ((map (wiki-+-mode-map)))
    (define-key map (kbd "C-c C-q") 'wiki-media-logout)
    (define-key map (kbd "C-c C-l") 'wiki-media-login ) map))

(wiki-define-mode 'media)

(provide 'wiki-media)

